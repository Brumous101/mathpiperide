v 1.23

This is a MathPiperIDE worksheet file, and it
consists of folds which contain source code. A
fold starts with percent<keyword> and ends with
percent/<keyword>. Code is entered inside a fold
and then evaluated by placing the cursor anywhere
inside the code and pressing <shift><enter>. The
result of the evaluation will be placed at the
bottom of the fold within an indented output fold.

Folds can be folded and unfolded by pressing the
small triangles in the gutter which is on the left
side of the text area. There are additional fold
operations in the "Folding" menu at the top of the
window.


%mathpiper,description="Perform a simple calculation."

5 + 6;

%/mathpiper







%mathpiper,title="Print the integers from 1 - 50."

x := 1;

While(x <=? 50)
{
  Write(x);
  
  WriteString(",");
  
  x := x + 1; 
}

%/mathpiper







%mathpiper,description="Solve an equation."

Solve(MathParse("2 - (-3x-8) = 1"), MathParse("x"));

%/mathpiper







%mathpiper,description="Solve numerous equations."

equs := 
[
["2 - (-3x-8) = 1","x"],
["-5(-4+2x)= -50","x"],
["66 = 6(6+5x)","x"],
["0 = -8(x-5)","x"],
["-2+2(8x-7)=-16","x"],
["-21x +12 = -6-3x","x"],
["-1-7x = -8x+7","x"],
["1-12r = 29 - 8r","r"],
["20 - 7x = -12x + 30","x"],
["-32-24x = 34 - 2x","x"],
["-2-5(2-4x) =33 + 5x","x"],
["-4x +  11 = 2(1 - 8x) + 3x","x"],
["-6x - 29 = -4x - 5(x + 1)","x"],
["2(4x-4) = -20 - 4x","x"],
["-x -5(8x-1) = 39 - 7x","x"],
["-57 = -(-x+1) + 2(6 + 8x)","x"],
];

ForEach(equ, equs)
{
    Echo(equ[1] + ": " + Nl() + Solve(MathParse(equ[1]), MathParse(equ[2])) + Nl());
}

%/mathpiper







%mathpiper,description="Working with units."

2~inch + 3~inch;

%/mathpiper







%mathpiper,title="Converting between units."

1~mile # inch;

%/mathpiper







%group,description="Automatically grade applied math problems."
========================================================================================================
Circular Motion and Force
(obtained from http://iweb.tntech.edu/murdock/books.html)

A 0.015 kg ball is shot from the plunger of a
pinball machine. Because of a centripetal force of
0.028 N, the ball follows a circular arc whose
radius is 0.25 m. What is the speed of the ball?


%mathpiper,name="Problem 1",subtype="problem",unassign_all="true",globalStateShow="true"

ProblemSolution([v?v])
{
    Givens()
    {
        F?C := .028~N;
        m?m := .015~kg;
        r?r := .25~m;
    }
    
    Formulas()
    {   
        f5?9 := Formula(F?C~N == (m?m~g * v?v~(m/s)^2) / r?r~m, 
                        Label:"5.9", 
                        Page:"73", 
                        Subject:'v?v);
    }

    v?v := EvaluateFormula(f5?9, m/s);
}

%/mathpiper





%mathpiper_grade,name="Problem 1",base_six_four="true"

CnsKICAgIEZvbGRHcmFkZSgiTWF0aFBpcGVyIHZlcnNpb24gPj0gMjg3IiwgMSwgVHJ1ZSkKICAgIHsKICAgICAgICBTdHJpbmdUb051bWJlcihWZXJzaW9uKCkpID49PyAyODc7CiAgICB9CgogICAgRm9sZEdyYWRlKCJUaGUgY29kZSBkb2VzIG5vdCB0aHJvdyBhbiBleGNlcHRpb24gd2hlbiBldmFsdWF0ZWQiLCAxLCBUcnVlKQogICAgewogICAgICAgIEV4Y2VwdGlvbkNhdGNoKCBQaXBlVG9TdHJpbmcoKSBFdmFsKD9mb2xkQ29kZVsxXSksICIiLCAnRXhjZXB0aW9uKSAhPT8gJ0V4Y2VwdGlvbjsKICAgIH0KCiAgICBGb2xkR3JhZGUoIkEgY29ycmVjdCBnaXZlbiB2YWx1ZSBpcyBhc3NpZ25lZCB0byAiICsgIkY/QyIsIDEsIEZhbHNlKQogICAgewogICAgICAgIEV4Y2VwdGlvbkNhdGNoKFVuaXRzRXF1YWw/KEY/QywgLjAyOH5OKSwgIiIsIEV4Y2VwdGlvbkdldCgpWyJtZXNzYWdlIl0pOwogICAgfQoKICAgIEZvbGRHcmFkZSgiQSBjb3JyZWN0IGdpdmVuIHZhbHVlIGlzIGFzc2lnbmVkIHRvICIgKyAibT9tIiwgMSwgRmFsc2UpCiAgICB7CiAgICAgICAgRXhjZXB0aW9uQ2F0Y2goVW5pdHNFcXVhbD8obT9tLCAuMDE1fmtnKSwgIiIsIEV4Y2VwdGlvbkdldCgpWyJtZXNzYWdlIl0pOwogICAgfQoKICAgIEZvbGRHcmFkZSgiQSBjb3JyZWN0IGdpdmVuIHZhbHVlIGlzIGFzc2lnbmVkIHRvICIgKyAicj9yIiwgMSwgRmFsc2UpCiAgICB7CiAgICAgICAgRXhjZXB0aW9uQ2F0Y2goVW5pdHNFcXVhbD8ocj9yLCAuMjV+bSksICIiLCBFeGNlcHRpb25HZXQoKVsibWVzc2FnZSJdKTsKICAgIH0KCiAgICBGb2xkR3JhZGUoIkZvcm11bGEgKDUuOSkgaXMgdXNlZCIsIDEsIEZhbHNlKQogICAgewogICAgICAgIChQb3NpdGlvbnNQYXR0ZXJuMihVbml0c1N0cmlwKD9mb2xkQ29kZSksICggJyhGP0NfID09ICgobT9tXyoodj92X14yKSkvcj9yXykpKSkgIT0/IFtdKTsKICAgIH0KCiAgICBGb2xkR3JhZGUoIkEgY29ycmVjdCB2YWx1ZSBpcyBhc3NpZ25lZCB0byAiICsgInY/diIgKyAiIHVzaW5nIHRoZSBFdmFsdWF0ZUZvcm11bGEgcHJvY2VkdXJlIiwgMSwgRmFsc2UpCiAgICB7CiAgICAgICAgRXhjZXB0aW9uQ2F0Y2goVW5pdHNFcXVhbD8odj92LCAwLjY4MzEzMDA1MTEzfihtL3MpKSwgIiIsIEV4Y2VwdGlvbkdldCgpWyJtZXNzYWdlIl0pOwogICAgfQp9Cgo=

%/mathpiper_grade

%/group







%group,name="Problem 3",description="Automatic grading of programs."
========================================================================================================

Problem 3

Create a program that uses a While() loop and a
NegativeNumber?() procedure to copy all of the
negative numbers in the following list into a new
list in the order which they appear in the
original list. Use the variable
"negativeNumbersList" to hold the new list. Return
the contents of the list after it has been
created. Do not assign the value from the input
list to a variable.

[36,-29,-33,-6,14,7,-16,-3,-14,37,-38,-8,-45,-21,-26,6,6,38,-20,33,41,-4,24,37,40,29]


%mathpiper,name="Problem 3",subtype="problem",unassign_all="true",globalStateShow="true",truncate="1500",timeout="5000"

list := [36,-29,-33,-6,14,7,-16,-3,-14,37,-38,-8,-45,-21,-26,6,6,38,-20,33,41,-4,24,37,40,29];

negativeNumbersList := [];

listLength := Length(list);

index := 1;

While(index <=? listLength)
{
    If(NegativeNumber?(list[index]))
    {
        Append!(negativeNumbersList, list[index]);
    }

    index := (index + 1);
}

negativeNumbersList;

%/mathpiper





%mathpiper_grade,name="Problem 3",base_six_four="true"

CkxvY2FsU3ltYm9scyhmb2xkUmVzdWx0KQp7CiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQogICAgCiAgICBGb2xkR3JhZGUoIlRoZSBjb2RlIGRvZXMgbm90IHRocm93IGFuIGV4Y2VwdGlvbiB3aGVuIGV2YWx1YXRlZCIsIDEsIFRydWUpCiAgICB7CiAgICAgICAgRXhjZXB0aW9uQ2F0Y2goZm9sZFJlc3VsdCA6PSBFdmFsKD9mb2xkQ29kZVsxXSksICIiLCAnRXhjZXB0aW9uKSAhPT8gJ0V4Y2VwdGlvbjsKICAgIH0KICAgIAogICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KICAgIAogICAgRm9sZEdyYWRlKCJNYXRoUGlwZXIgdmVyc2lvbiA+PSAuMjU3IiwgMSwgVHJ1ZSkKICAgIHsKICAgICAgICBTdHJpbmdUb051bWJlcihWZXJzaW9uKCkpID49PyAuMjU3OwogICAgfQogICAgCiAgICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tICAgIAogICAgCiAgICBGb2xkR3JhZGUoIlRoZSBjb2RlIGRvZXMgbm90IHByb2R1Y2Ugc2lkZSBlZmZlY3Qgb3V0cHV0IiwgMSwgRmFsc2UpCiAgICBMb2NhbFN5bWJvbHMocHJvY2VkdXJlTmFtZXMpCiAgICB7CiAgICAgICAgcHJvY2VkdXJlTmFtZXMgOj0gUHJvY2VkdXJlTGlzdCg/Zm9sZENvZGUpOwogICAgICAgIAogICAgICAgICE/IENvbnRhaW5zPyhwcm9jZWR1cmVOYW1lcywiRWNobyIpICY/ICE/IENvbnRhaW5zPyhwcm9jZWR1cmVOYW1lcywiV3JpdGUiKSAmPyAhPyBDb250YWlucz8ocHJvY2VkdXJlTmFtZXMsIlRhYmxlRm9ybSIpOwogICAgfQogICAgCiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQogICAgCiAgICBGb2xkR3JhZGUoIlRoZSBBcHBlbmQhKCkgcHJvY2VkdXJlIGlzIHVzZWQiLCAxLCBGYWxzZSkKICAgIExvY2FsU3ltYm9scyhwcm9jZWR1cmVOYW1lcykKICAgIHsKICAgICAgICBwcm9jZWR1cmVOYW1lcyA6PSBQcm9jZWR1cmVMaXN0KD9mb2xkQ29kZSk7CiAgICAgICAgQ29udGFpbnM/KHByb2NlZHVyZU5hbWVzLCJBcHBlbmQhIik7CiAgICB9CiAgICAKICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCiAgICAKICAgIEZvbGRHcmFkZSgiVGhlIEFwcGVuZCEoKSBwcm9jZWR1cmUncyBmaXJzdCBhcmd1bWVudCBpcyAnbmVnYXRpdmVOdW1iZXJzTGlzdCciLCAxLCBGYWxzZSkKICAgIHsKICAgICAgICBQb3NpdGlvbnNQYXR0ZXJuMig/Zm9sZENvZGUsICcoIEFwcGVuZCEoYV8sIGJfKTo6KGEgPT8gJ25lZ2F0aXZlTnVtYmVyc0xpc3QpKSkgIT0/IFtdOwogICAgfQogICAgCiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQogICAgCiAgICBGb2xkR3JhZGUoIlRoZSBBcHBlbmQhKCkgcHJvY2VkdXJlJ3Mgc2Vjb25kIGFyZ3VtZW50IGFjY2Vzc2VzIGFuIGVsZW1lbnQgaW4gdGhlIGlucHV0IGxpc3QuIFRoZSBzZWNvbmQgYXJndW1lbnQgaXMgbm90IHRoZSB3aG9sZSBsaXN0LCBhbmQgaXQgaXMgbm90IGp1c3QgYW4gaW5kZXgiLCAxLCBGYWxzZSkKICAgIHsKICAgICAgICBQb3NpdGlvbnNQYXR0ZXJuMig/Zm9sZENvZGUsICcoIEFwcGVuZCEoYV8sIGJfKTo6KFR5cGUoYikgPT8gIk50aCIpKSkgIT0/IFtdOwogICAgfQogICAgCiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQogICAgCiAgICBGb2xkR3JhZGUoIlRoZSByZXN1bHQgaXMgYSBsaXN0IiwgMSwgVHJ1ZSkKICAgIHsKICAgICAgICBMaXN0Pyhmb2xkUmVzdWx0KTsKICAgIH0KICAgIAogICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KICAgIAogICAgRm9sZEdyYWRlKCJUaGUgZXhwcmVzc2lvbiAnbmVnYXRpdmVOdW1iZXJzTGlzdDsnIGlzIHRoZSBsYXN0IGV4cHJlc3Npb24gaW4gdGhlIGZvbGQiLCAxLCBGYWxzZSkKICAgIExvY2FsU3ltYm9scyhtYWluUG9zaXRpb24pCiAgICB7CiAgICAgICAgP2ZvbGRDb2RlWzFdW0xlbmd0aCg/Zm9sZENvZGVbMV0pXSA9PyAnbmVnYXRpdmVOdW1iZXJzTGlzdDsKICAgIH0KICAgIAogICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KICAgIAoKICAgIEZvbGRHcmFkZSgiVGhlIHJlc3VsdCBpcyBub3QgaW4gdGhlIGNvZGUgYXMgYSBsaXRlcmFsIiwgMSwgVHJ1ZSkKICAgIExvY2FsU3ltYm9scyh2YWx1ZXMpCiAgICB7ICAKICAgICAgICB2YWx1ZXMgOj0gU3VidHJlZXNQYXR0ZXJuKD9mb2xkQ29kZSwgYV9MaXN0PyApOwoKICAgICAgICAhPyBDb250YWlucz8odmFsdWVzLCAiWy0yOSwtMzMsLTYsLTE2LC0zLC0xNCwtMzgsLTgsLTQ1LC0yMSwtMjYsLTIwLC00XSIpOwogICAgfQogICAgCiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQogICAgCiAgICBGb2xkR3JhZGUoIlRoZSBjb3JyZWN0IHZhbHVlIGlzIHJldHVybmVkIiwgMSwgVHJ1ZSkKICAgIHsKICAgICAgICBmb2xkUmVzdWx0ID0/IFstMjksLTMzLC02LC0xNiwtMywtMTQsLTM4LC04LC00NSwtMjEsLTI2LC0yMCwtNF07CiAgICB9CiAgICAKICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCiAgICAKICAgIEZvbGRHcmFkZSgiVGhlIHByb2NlZHVyZSBXaGlsZSgpIGlzIHVzZWQgaW4gdGhlIGNvZGUiLCAxLCBGYWxzZSkKICAgIExvY2FsU3ltYm9scyhwcm9jZWR1cmVOYW1lcykKICAgIHsKICAgICAgICBwcm9jZWR1cmVOYW1lcyA6PSBQcm9jZWR1cmVMaXN0KD9mb2xkQ29kZSk7CiAgICAgICAgQ29udGFpbnM/KHByb2NlZHVyZU5hbWVzLCJXaGlsZSIpICY/ICE/IENvbnRhaW5zPyhwcm9jZWR1cmVOYW1lcywiVW50aWwiKTsKICAgIH0KfQoK

%/mathpiper_grade

%/group







%html,width="700",height="440",description="Rendering HTML"

	<html>
		<h1 align="center">HTML Color Values</h1>
		<table border="0" cellpadding="10" cellspacing="1" width="600">
			<tr>
				<th bgcolor="white" colspan="2"></th>
				<th colspan="6">where blue=cc</th>
			</tr>
			<tr>
				<th rowspan="6">where&nbsp;red=</th>
				<th>ff</th>
				<th bgcolor="#ff00cc">ff00cc</th>
				<th bgcolor="#ff33cc">ff33cc</th>
				<th bgcolor="#ff66cc">ff66cc</th>
				<th bgcolor="#ff99cc">ff99cc</th>
				<th bgcolor="#ffcccc">ffcccc</th>
				<th bgcolor="#ffffcc">ffffcc</th>
			</tr>
			<tr>
				<th>cc</th>
				<th bgcolor="#cc00cc">cc00cc</th>
				<th bgcolor="#cc33cc">cc33cc</th>
				<th bgcolor="#cc66cc">cc66cc</th>
				<th bgcolor="#cc99cc">cc99cc</th>
				<th bgcolor="#cccccc">cccccc</th>
				<th bgcolor="#ccffcc">ccffcc</th>
			</tr>
			<tr>
				<th>99</th>
				<th bgcolor="#9900cc">
					<font color="#ffffff">9900cc</font>
				</th>
				<th bgcolor="#9933cc">9933cc</th>
				<th bgcolor="#9966cc">9966cc</th>
				<th bgcolor="#9999cc">9999cc</th>
				<th bgcolor="#99cccc">99cccc</th>
				<th bgcolor="#99ffcc">99ffcc</th>
			</tr>
			<tr>
				<th>66</th>
				<th bgcolor="#6600cc">
					<font color="#ffffff">6600cc</font>
				</th>
				<th bgcolor="#6633cc">
					<font color="#FFFFFF">6633cc</font>
				</th>
				<th bgcolor="#6666cc">6666cc</th>
				<th bgcolor="#6699cc">6699cc</th>
				<th bgcolor="#66cccc">66cccc</th>
				<th bgcolor="#66ffcc">66ffcc</th>
			</tr>
			<tr>
				<th colspan="1"></th>
				<th>00</th>
				<th>33</th>
				<th>66</th>
				<th>99</th>
				<th>cc</th>
				<th>ff</th>
			</tr>
			<tr>
				<th colspan="2"></th>
				<th colspan="4">where green=</th>
			</tr>
		</table>
	</html>
    
%/html
