%mathpiper,def="TruthTable"

TruthTable(booleanExpression) :=
{
    Local(resultList, variables, booleanPatterns, subexpressions, substitutionList, evaluation);
    
    resultList := [];
    
    variables := VarList(booleanExpression);
    
    booleanPatterns := Reverse(BooleanLists(Length(variables)));
    
    subexpressions := Subexpressions(booleanExpression);
    
    Append!(resultList, subexpressions);
    

    ForEach(booleanPattern, booleanPatterns)
    {
        substitutionList := Map("==", [variables, booleanPattern]);
        
        evaluation := `(subexpressions Where @substitutionList);
        
        Append!(resultList, evaluation);
    
    };
    
    resultList;
};

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output




%mathpiper_docs,name="TruthTable",categories="Mathematics Functions;Propositional Logic",access="experimental"
*CMD TruthTable --- returns a list that contains a truth table for a boolean expression

*CALL
        TruthTable(booleanExpression)

*PARMS
{booleanExpression} -- a boolean expression.

*DESC
Returns a list that contains a truth table for a boolean expression. The truth table is returned as a 
list of lists.

*E.G.
In> za := TruthTable(a And? b)
Result: [[a,b,a And? b],[True,True,True],[True,False,False],[False,True,False],[False,False,False]]

//Substitute T and F for True and False.
In> zb := (za /: [True <- T, False <- F])
Result: [[a,b,a And? b],[T,T,T],[T,F,F],[F,T,F],[F,F,F]]

//View the table in traditional mathematics format.
In> ViewMath(zb)
Result: class javax.swing.JFrame

*SEE Subexpressions, BooleanList, BooleanLists

%/mathpiper_docs

    %output,preserve="false"
      
.   %/output





%mathpiper,name="TruthTable",subtype="automatic_test"

Unassign(a,b);

Verify(TruthTable(a And? b), [[a,b,a And? b],[True,True,True],[True,False,False],[False,True,False],[False,False,False]]);

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output


