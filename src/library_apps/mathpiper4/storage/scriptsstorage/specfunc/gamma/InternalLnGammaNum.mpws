%mathpiper,def="InternalLnGammaNum"

/////////////////////////////////////////////////
/// Euler's Gamma function
/////////////////////////////////////////////////

//Serge Winitzki

/// This procedure computes the uniform approximation for the Gamma function
/// due to Lanczos and Spouge (the so-called "less precise coefficients")
/// evaluated at arbitrary precision by using a large number of terms
/// See J. L. Spouge, SIAM J. of Num. Anal. 31, 931 (1994)
/// See also Paul Godfrey 2001 (unpublished): http://winnie.fit.edu/~gabdo/gamma.txt for a discussion

/// Calculate the uniform approximation to the logarithm of the Gamma function
/// in the Re z > 0 half-plane; argument z may be symbolic or complex
/// but current value of precision is used
/// Note that we return LnGamma(z), not of z+1
/// This function should not be used directly by applications
10 # InternalLnGammaNum(_z, _a)_(NM(Re(z))<?0) <-- {
        Decide(InVerboseMode(), Echo(["InternalLnGammaNum: using 1-z identity"]));
        NM(Ln(Pi/Sin(Pi*z)) - InternalLnGammaNum(1-z, a));
};
20 # InternalLnGammaNum(_z, _a) <-- {
        Local(e, k, tmpcoeff, coeff, result);
        a := Maximum(a, 4);        // guard against low values
        Decide(InVerboseMode(), Echo(["InternalLnGammaNum: precision parameter = ", a]));
        e := NM(Exp(1));
        k:=Ceil(a);        // prepare k=N+1; the k=N term is probably never significant but we don't win much by excluding it
        result := 0;        // prepare for last term
        // use Horner scheme to prevent loss of precision
        While(k>?1) {        // 'result' will accumulate just the sum for now
                k:=k-1;
                result := NM( PowerN(a-k,k)/((z+k)*Sqrt(a-k))-result/(e*k) );
        };
        NM(Ln(1+Exp(a-1)/Sqrt(2*Pi)*result) + Ln(2*Pi)/2 -a-z+(z+1/2)*Ln(z+a) - Ln(z));
};

InternalLnGammaNum(z) := {
        Local(a, prec, result);
        prec := BuiltinPrecisionGet();
        a:= Quotient((prec-IntLog(prec,10))*659, 526) + 0.4;        // see algorithm docs
        /// same as parameter "g" in Godfrey 2001.
        /// Chosen to satisfy Spouge's error bound:
        /// error < Sqrt(a)/Real(a+z)/(2*Pi)^(a+1/2)
//        Echo(["parameter a = ", a, " setting precision to ", Ceil(prec*1.4)]);
        BuiltinPrecisionSet(Ceil(prec*1.4));        // need more precision b/c of roundoff errors but don't know exactly how many digits
        result := InternalLnGammaNum(z,a);
        BuiltinPrecisionSet(prec);
        result;
};


%/mathpiper