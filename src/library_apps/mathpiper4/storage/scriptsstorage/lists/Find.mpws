%mathpiper,def="Find"

10 # Find( list_List?, _element ) <--
  {
     Local(result,count);
     result := -1;
     count  := 1;
     While( And?( result <? 0, Not? ( Equal? (list, []) )))
     {
       Decide(Equal?(First(list), element), result := count );
       list := Rest(list);
       count++;
     };
     result;
  };

%/mathpiper



%mathpiper_docs,name="Find",categories="Programming Functions;Lists (Operations)"
*CMD Find --- get the index at which a certain element occurs
*STD
*CALL
        Find(list, expr)

*PARMS

{list} -- the list to examine

{expr} -- expression to look for in "list"

*DESC

This commands returns the index at which the expression "expr"
occurs in "list". If "expr" occurs more than once, the lowest
index is returned. If "expr" does not occur at all,
{-1} is returned.
NOTE: Find is a synonym for FindFirst.

*E.G.

In> Find([a,b,c,d,e,f], d);
Result: 4;

In> Find([1,2,3,2,1], 2);
Result: 2;

In> Find([1,2,3,2,1], 4);
Result: -1;

*SEE FindFirst, FindAll, Contains?
%/mathpiper_docs


