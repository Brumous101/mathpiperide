%mathpiper,def="Polynomial?"


10 # Polynomial?( expr_String? ) <-- False;

15 # Polynomial?( expr_Constant? ) <-- True;
 
20 # Polynomial?(_expr) <-- 
{
    Local(x,vars);
    vars := VarList(expr);
    Decide(Length(vars)>?1,vars:=HeapSort(vars,"GreaterThan?"));    
    x := vars[1];
    Polynomial?(expr,x);
};

25 # Polynomial?(_expr) <-- False;


10 # Polynomial?(_expr,_var)_(CanBeUni(var,expr)) <-- True;

15 # Polynomial?(_expr,_var) <-- False;

%/mathpiper



%mathpiper_docs,name="Polynomial?",categories="Programming Functions;Predicates"

*CMD Polynomial? --- Check if [expr] is a polynomial in variable [var] if [var] is specified.

*STD
*CALL
    Polynomial?(expr,var)
or
    Polynomial?(expr)

*PARMS

{expr} -- an algebraic expression which may be a polynomial

{var}  -- a variable name which might be used in {expr}

*DESC

The command {Polynomial?} returns {True} if {expr} is (or could be) a polynomial in {var}.
If {var} is not specified, a heuristic algorithm (which may be wrong!) is used to select a
likely variable name from among the list of "variables" returned by VarList(expr).
If you would rather not have an algorithm selecting the variable name, specify it as an
argument to the function. It returns {False} if {expr} is not likely to be a polynomial 
in {var}.

*E.G.

In> Polynomial?(2*x^3-3*x^2+5*x-14,x)
Result: True 

In> Polynomial?(2*x^3-3*x^2+5*x-14)
Result: True

In> Polynomial?(y^2-4)
Result: True
        NOTE: if variable name is omitted, a reasonable default is taken.

%/mathpiper_docs


