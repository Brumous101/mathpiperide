package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.StaticBitmapButton;
import java.awt.event.MouseEvent;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

public class SwitchDCACGND extends StaticBitmapButton
{

    public static final int GND = 0;
    public static final int AC = 1;
    public static final int DC = 2;
    public static final boolean IZQ = true;
    public static final boolean DER = false;
    private boolean posLetters;
    private int state;

    public SwitchDCACGND(String Files[])
    {
        super(Files);
        state = 2;
        posLetters = true;
        super.posImage = state;
    }

    public void setJustificacion(boolean pos)
    {
        posLetters = pos;
    }

    public void mouseClicked(MouseEvent e)
    {
        int x = e.getX();
        int y = e.getY();
        if(y < getSize().height / 3)
        {
            state = 2;
        } else
        if(y < (getSize().height * 2) / 3)
        {
            state = 1;
        } else
        {
            state = 0;
        }
        super.posImage = state;
        draw(getGraphics());
    }

    public Dimension getMinimumSize()
    {
        int width = super.Image[0].getWidth(this);
        int height = super.Image[0].getHeight(this);
        FontMetrics font = getFontMetrics(getFont());
        width += font.stringWidth("DC");
        return new Dimension(width, height);
    }

    public int getValue()
    {
        return state;
    }

    public void setValue(int e)
    {
        state = e;
        super.posImage = e;
        repaint();
    }

    public void draw(Graphics g)
    {
        int width = getSize().width;
        int height = getSize().height;
        FontMetrics font = getFontMetrics(getFont());
        int x = font.stringWidth("DC");
        if(posLetters)
        {
            g.drawImage(super.Image[super.posImage], x, 0, this);
            x = 0;
        } else
        {
            g.drawImage(super.Image[super.posImage], 0, 0, this);
            x = super.Image[super.posImage].getWidth(this);
        }
        if(state == 2)
        {
            g.setColor(Color.red);
        } else
        {
            g.setColor(Color.orange);
        }
        g.drawString("DC", x, getSize().height / 3);
        if(state == 1)
        {
            g.setColor(Color.red);
        } else
        {
            g.setColor(Color.orange);
        }
        g.drawString("AC", x, (getSize().height * 2) / 3);
        if(state == 0)
        {
            g.setColor(Color.red);
        } else
        {
            g.setColor(Color.orange);
        }
        g.drawString("GD", x, getSize().height);
    }
}
