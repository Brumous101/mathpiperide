
package org.mathpiper.ui.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.Box;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;


public class VerticalLine extends LayoutTechnique {
    
    private Box box;
            
    public VerticalLine()
    {
        this.setLayout(new BorderLayout());
        
        box = Box.createVerticalBox();
        
        box.setBorder(BorderFactory.createLineBorder(Color.black));

        this.add(box);
    }
    
    public void setList(Environment aEnvironment, int aStackTop, Cons dataList) throws Throwable
    {
        box.removeAll();
        
        //box.add(new VariableWidthLine("LINE"));
        
        while (dataList != null)
        {
            Object object = dataList.car();

            if (!(object instanceof JavaObject))
            {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }

            JavaObject javaObject = (JavaObject) object;

            object = javaObject.getObject();

            if (!(object instanceof Component))
            {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }

            Component component = (Component) object;
            
            if (component.getName() != null && component.getName().equals("LINE")) {
                box.add(new VariableWidthLine("LINE"));
            }
            else
            {
                box.add(component);
            }

            dataList = dataList.cdr();
        }
    }
    
}
