package org.mathpiper.ui.gui;

import java.awt.Graphics;
import javax.swing.JComponent;


class VariableWidthLine extends JComponent
{
    public VariableWidthLine(String name)
    {
        this.setName(name);
    }
    
    public void paintComponent(Graphics g) {
            
        super.paintComponent(g);
        
        g.drawLine(0, 0, g.getClipBounds().width, 0);
        
    }
}
