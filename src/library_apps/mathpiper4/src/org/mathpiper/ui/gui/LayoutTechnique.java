
package org.mathpiper.ui.gui;

import javax.swing.JPanel;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;


public abstract class LayoutTechnique extends JPanel {
    
    public abstract void setList(Environment aEnvironment, int aStackTop, Cons dataList) throws Throwable;
            
    
}
