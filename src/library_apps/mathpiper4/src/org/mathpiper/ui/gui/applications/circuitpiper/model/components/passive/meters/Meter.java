package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.DrawingPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public abstract class Meter extends Component {

    public static int METER_SIZE = 27;
    
    public static int componentCounter = 1;

    public Meter(int x, int y, CircuitPanel circuitPanel) {
        super(x, y, circuitPanel);
        componentUID = componentCounter++ + "";
        primarySymbol = "M";
    }
    
    public void reset()
    {
        secondaryValue = 0;
        fullValue = "";
        calculatedValue = 0.0;
    }

    public void draw(ScaledGraphics sg) {
        super.draw(sg);
        int x1 = headTerminal.getX();
        int x2 = tailTerminal.getX();
        int y1 = headTerminal.getY();
        int y2 = tailTerminal.getY();
        double xm = (x1 + x2) / 2;
        double ym = (y1 + y2) / 2;
        sg.drawString("Meter",  xm - 15,  ym + 15);
    }
    
    public int getLabelDistance()
    {
        return 30;
    }
}
