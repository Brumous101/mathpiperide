package org.mathpiper;

import java.util.HashMap;

//*** GENERATED FILE, DO NOT EDIT ***

import java.util.Map;

public class Tests {

    private HashMap userProceduresTestsMap = null;

    private HashMap builtInProceduresTestsMap = null;

    private HashMap documentationExamplesTestsMap = null;

    public Tests() {

        userProceduresTestsMap = new HashMap();

        builtInProceduresTestsMap = new HashMap();

        documentationExamplesTestsMap = new HashMap();
    }

    public String[] getUserProcedureScript(String testName)
    {
        return (String[]) userProceduresTestsMap.get(testName);
    }

    public Map getUserProceduresMap()
    {
        return userProceduresTestsMap;
    }

    public String[] getBuiltInProcedureScript(String testName)
    {
        return (String[]) builtInProceduresTestsMap.get(testName);
    }

    public Map getBuiltInProceduresMap()
    {
        return builtInProceduresTestsMap;
    }

    public String[] getdocumentationExamplesTestScript(String testName)
    {
        return (String[]) documentationExamplesTestsMap.get(testName);
    }

    public Map getdocumentationExamplesTestsMap()
    {
        return documentationExamplesTestsMap;
    }
}
