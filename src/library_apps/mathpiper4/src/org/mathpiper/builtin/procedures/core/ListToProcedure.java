/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;


/**
 *
 *  
 */
public class ListToProcedure extends BuiltinProcedure
{

    private ListToProcedure()
    {
    }

    public ListToProcedure(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        if(getArgument(aEnvironment, aStackTop, 1) == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        if(! (getArgument(aEnvironment, aStackTop, 1).car() instanceof Cons)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Cons atom = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
        if( atom == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        if(! (((String)atom.car()).equals((String)aEnvironment.iListAtom.car()))) LispError.checkArgument(aEnvironment, aStackTop, 1);
      
        Cons tail = Utility.tail(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1));
        
        /*
        if(((String) tail.car()).startsWith("_"))
        {
            tail.setCar(((String) tail.car()).substring(1,((String) tail.car()).length()));
        }
        */
        
        setTopOfStack(aEnvironment, aStackTop, tail);
    }
}



/*
%mathpiper_docs,name="ListToProcedure",categories="Programming Procedures,Lists (Operations),Built In"
*CMD ListToProcedure --- convert a list to a procedure
*CORE
*CALL
	ListToProcedure(list)

*PARMS

{list} -- list to be converted

*DESC

This command converts a list to a procedure. The car
entry of "list" is treated as a procedure atom, and the following entries
are the arguments to this procedure. So the procedure referred to in the
car element of "list" is applied to the other elements.

Note that "list" is evaluated before the procedure application is
formed, but the resulting expression is left unevaluated. The procedures 
{ListToProcedure()} and {Hold()} both stop the process of evaluation.

*E.G.

In> ListToProcedure('[Cos, x]);
Result: Cos(x);

In> ListToProcedure('[f]);
Result: f();

In> ListToProcedure('[Taylor,x,0,5,Cos(x)]);
Result: Taylor(x,0,5)Cos(x);


*SEE List, ProcedureToList, Hold
%/mathpiper_docs





%mathpiper,name="ListToProcedure",subtype="automatic_test"

// ProcedureToList and ListToProcedure coredumped when their arguments were invalid.
Verify(ListToProcedure('[Sqrt,_x]),Sqrt(_x));

{
  Local(exception);
 
  exception := False;
  ExceptionCatch(ListToProcedure(1.2), "", exception := ExceptionGet());
  Verify(exception =? False, False);
};

%/mathpiper
*/