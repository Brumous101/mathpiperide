/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.interpreters.ResponseProvider;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.MathPanel;
import org.mathpiper.ui.gui.worksheets.TreePanelCons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *
 */
public class Assess extends BuiltinProcedure implements ResponseListener
{
    private Map defaultOptions;
    private final AtomicReference<String> userInput = new AtomicReference<String>();
    private final AtomicReference<Long> startTime = new AtomicReference<Long>();

    private JDialog dialog;
    
    

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "Assess";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        
        defaultOptions = new HashMap();
        defaultOptions.put("Scale", 1.0);
        defaultOptions.put("Resizable", false);
        defaultOptions.put("Message", "");
    
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.
        
        Cons options = arguments.cdr();
        final Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        
        Object firstArgument = arguments.car();

        if (firstArgument instanceof JavaObject && ((JavaObject) firstArgument).getObject() instanceof List)
        {
            List resultList = (List) ((JavaObject) firstArgument).getObject();

            Object object = resultList.get(0);

            if (object instanceof Component)
            {
                final Component componentFinal = (Component) object;

                Object controller = resultList.get(1);

                if (controller != null && controller instanceof ResponseProvider)
                {
                    ResponseProvider responseProvider = (ResponseProvider) controller;
                    responseProvider.addResponseListener(this);
                }
                

                userInput.set("NO_INPUT_RECEIVED");
                
                startTime.set(System.nanoTime());
                                        
                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        dialog = new JDialog();
                        dialog.setTitle((String) userOptions.get("Message"));
                        dialog.setModal(true);
                        JPanel panel = new JPanel();
                        panel.setLayout(new BorderLayout());
                        panel.add(componentFinal);
                        dialog.setContentPane(panel);
                        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                        dialog.pack();
                        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                        int width = screenSize.width * 7/10;
                        int height = screenSize.height * 7/10;
                        dialog.setSize(width, height);
                        dialog.setLocationRelativeTo(null); 
                        dialog.setVisible(true);
                    }
                });

                while (userInput.get() != null && ((String) userInput.get()).equals("NO_INPUT_RECEIVED"))
                {
                    try
                    {
                        Thread.sleep(100);
                    } catch (InterruptedException e)
                    {
                        //Eat exception.
                    }
                }

                //Window window = SwingUtilities.getWindowAncestor(componentFinal);
                //window.setVisible(false);
                
                dialog.setVisible(false);

                setTopOfStack(aEnvironment, aStackTop, Utility.mathPiperParse(aEnvironment, aStackTop, userInput.get())); //AtomCons.getInstance(aEnvironment.getPrecision(), aStackTop, "\"" + userInput.get() + "\""));
            }
        } else
        {
            throw new Exception("Internal error.");
        }
    }//end method.

    public void response(EvaluationResponse response)
    {
        long duration = System.nanoTime() - startTime.get();
        String result = response.getResult();
        userInput.set("[[\"Result\",\"" + result + "\"], [\"Duration\", " + duration + "]];");
    }

    public boolean remove()
    {
        return false;
    }
}//end class.

/*
 %mathpiper_docs,name="Assess",categories="Programming Procedures,Assessment"
 *CMD AskUser --- displays a dialog that contains a Java Component
 *CORE
 *CALL
 Assess(javaComponent)

 *PARMS

 {javaObject} -- a Java Component

 *DESC
Shows a Java Component to the user that has parts which
can be selected with the mouse.

 %/mathpiper_docs
 */
