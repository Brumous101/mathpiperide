%mathpiper,def="Formula;FormulaHelper"

Retract("Formula", All);
Retract("FormulaHelper", All);

RulebaseListedHoldArguments("Formula", ["formula"]);
RulebaseListedHoldArguments("Formula", ["formula", "optionsList"]);
HoldArgument("Formula", "formula");
HoldArgument("Formula", "optionsList");



/*

//Handle no options call.
5 ## Formula(formula_) <-- `Formula(@formula[1], []);


//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
Formula(formula_, optionsList_List?) <--
{
}


//Handle a single option call because the option does not come in a list for some reason.
20 ## Formula(formula_, singleOption_) <-- `Formula(@formula, [singleOption]);
*/


//=======================

RulebaseListedHoldArguments("FormulaHelper", ["formula"]);
RulebaseListedHoldArguments("FormulaHelper", ["formula", "optionsList"]);
HoldArgument("FormulaHelper", "formula");


//Handle no options call.
5 ## FormulaHelper(formula_) <-- `FormulaHelper(@formula, []);


//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
10 ## FormulaHelper(formula_, optionsList_List?) <--
{
    If(Verbose?())
    {
        Echo("Formula");
    }
   
    Local(options, definedOptionKeys, optionKeys, substitutions, subject, strippedFormula);
       
    definedOptionKeys := ["Subject", "Description", "Substitutions", "Label", "Page"];

    options := OptionsToAssociationList(optionsList);
    
    ForEach(option, optionsList)
    {
        If(option[1] =? 'Substitutions)
        {
            ForEach(substitution, option[2])
            {
                If(PositionsPattern2(substitution, a_Number?) !=? [])
                {
                    ExceptionThrow("", "Numbers cannot be used when substituting.", MetaData:substitution);
                }
            }
        }
    }
   
    ForEach(option, options)
    {
        If(!? Contains?(definedOptionKeys, option[1]))
        {
            ExceptionThrow("", "The option name \"" + option[1] + "\" is not defined.");
        }
    }
    
    options["OriginalFormula"] := formula;
    options["Transformed?"] := False;

    subject := Decide(Atom?(options["Subject"]), options["Subject"], None);
    substitutions := Decide(List?(options["Substitutions"]), options["Substitutions"], None);
  
    strippedFormula := UnitsStrip(formula);
  
    If(substitutions !=? None)
    {
        ForEach(substitution, substitutions)
        {
            If(Type(substitution) !=? "<-")
            {
                ExceptionThrow("", "Each substitution must be specified with a \"<-\" operator.", MetaData:substitution);
            }
            
            strippedFormula := Substitute(Eval(substitution[1]), Eval(substitution[2])) strippedFormula;
        }
        
        options["Transformed?"] := True;
    }

    If(subject !=? None)
    {
        Local(solution);
        
        If(Type(strippedFormula) =? "==")
        {
            solution := Solve(strippedFormula, subject);
        }
        Else
        {
            solution := Solve(Eval(strippedFormula), subject);
        }
        
        If(solution =? [])
        {
            ForEach(option, optionsList)
            {
                If(Type(option) =? ":" &? option[1] =? 'Subject)
                {
                    ExceptionThrow("", "The variable \"" + subject + "\" does not exist in the formula.", MetaData:option);
                }
            }
        }
        Else
        {
            options["FinalFormula"] := solution[1];
            options["Transformed?"] := True;
        }
    }
    Else
    {
        options["FinalFormula"] := strippedFormula;
    }
    
    options;
}


//Handle a single option call because the option does not come in a list for some reason.
20 ## FormulaHelper(formula_, singleOption_) <-- `FormulaHelper(@formula, [singleOption]);


%/mathpiper







%mathpiper_docs,name="Formula",categories="Programming Procedures,Miscellaneous"
*CMD Formula --- define a formula in a ProblemSolution body

*CALL
        Formula(formula, <options>)

*PARMS

{formula} -- a formula


*DESC

Evaluate an expression inside of a {Formulas} body.

*EXAMPLES

*SEE ProblemSolution, Formulas, Givens, EvaluateFormula

%/mathpiper_docs





%mathpiper,name="Formula",subtype="in_prompts"

formulaName := Formula(z == x + y, Label: "Example Formula") -> Formula(z == x + y, Label: "Example Formula")

f3?1 := Formula(I?OL~A == V?OL~V/R?OL~Ω, Label: "3.1", Page: "78", Description: "Find current using Ohm's Law") -> Formula(I?OL~A == V?OL~V/R?OL~Ω, Label:"3.1", Page:"78", Description:"Find current using Ohm's Law")

f3?1a := Formula(f3?1, Label: "3.1a", Page: "78", Description: "Find voltage using Ohm's Law", Subject: 'V?OL) -> Formula(f3?1, Label:"3.1a", Page:"78", Description:"Find voltage using Ohm's Law", Subject: 'V?OL)

f3?1b := Formula(f3?1, Label: "3.1b", Description: "Ohm's Law with substituted variable names", Substitutions: ['V?OL <- 'V?V, 'R?OL <- 'R?R, 'I?OL <- 'I?I]) -> Formula(f3?1, Label:"3.1b", Description:"Ohm's Law with substituted variable names", Substitutions:[ 'V?OL <- 'V?V, 'R?OL <- 'R?R, 'I?OL <- 'I?I])

%/mathpiper
