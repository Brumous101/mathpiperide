%mathpiper,def="HoldArgumentNumber"

Procedure("HoldArgumentNumber",["procedure", "arity", "index"])
{
  Local(args);
  args:=RulebaseArgumentsList(procedure,arity);
/* Echo(["holdnr ",args]); */
  ApplyFast("HoldArgument",[procedure,args[index]]);
}

%/mathpiper



%mathpiper_docs,name="HoldArgumentNumber",categories="Programming Procedures,Miscellaneous,Built In"
*CMD HoldArgumentNumber --- specify argument as not evaluated
*STD
*CALL
        HoldArgumentNumber("procedure", arity, argNum)

*PARMS
{"procedure"} -- string, procedure name

{arity}, {argNum} -- positive integers

*DESC

Declares the argument numbered {argNum} of the procedure named {"procedure"} with
specified {arity} to be unevaluated ("held"). Useful if you don't know symbolic
names of parameters, for instance, when the procedure was not defined using an
explicit {RulebaseHoldArguments} call. Otherwise you could use {HoldArgument}.
  
Note that defining rules with the {<--} operator can also result
in a rulebase with the implied arity being defined. Therefore,
{HoldArgument} should be placed after all rule definitions if all possible
rulebases are not defined by {Rulebase} procedures.

*SEE HoldArgument, RulebaseHoldArguments
%/mathpiper_docs