%mathpiper,def="NthRoot"

/* def file definitions
NthRoot
NthRootCalc
NthRootList
NthRootSave
NthRootRestore
NthRootClear

*/

//////
// $Id: nthroot.mpi,v 1.5 2007/05/17 11:56:45 ayalpinkus Exp $
// calculation/simplifaction of nth roots of nonnegative integers
// NthRoot         - interface procedure
// NthRootCalc    - actually calculate/simplifies
// NthRootList    - list table entries for a given n
// NthRootRestore - get a root from lookup table
// NthRootSave    - save a root in lookup table
// NthRootClear   - clear lookup table
//////

// LocalSymbols(m,n,r,
//                NthRootTable,
//                NthRootCalc,
//                NthRootList,
//                NthRootRestore,
//                NthRootSave,
//                NthRootClear)
LocalSymbols(m,n,r,
             NthRootTable)
{

// interface procedure for nth root of m
// m>=0, n>1, integers
// m^(1/n) --> f*(r^(1/n))
NthRoot(m_NonNegativeInteger?,n_Integer?)::(n>?1) <--
{
   Local(r);
   r:=NthRootRestore(m,n);
   Decide(Length(r)=?0,
   {
      r:=NthRootCalc(m,n);
      NthRootSave(m,n,r);
   });
   r;
}

// internal procedures
Procedure("NthRootCalc",["m", "n"])
{
   Local(i,j,f,r,in);
   Assign(i,2);
   Assign(j,Ceil(FastPower(m,NM(1.0/n))+1));
   Assign(f,1);
   Assign(r,m);
   // for large j (approx >4000)
   // using Factors instead of the
   // following.  would this be
   // faster in general?
//Echo("i j ",i," ",j);
   While(LessThan?(i,j))
   {
      Assign(in,PowerN(i,n));
//Echo("r in mod ",r, " ",in," ",ModuloN(r,in));
      While(Equal?(ModuloN(r,in),0))
      {
         Assign(f,MultiplyN(f,i));
         Assign(r,QuotientN(r,in));
      }
      While(Equal?(ModuloN(r,i),0))   //
         Assign(r,QuotientN(r,i));         //
      //Assign(i,NextPrime(i));
      Assign(i,NextPseudoPrime(i));
      Assign(j,Ceil(FastPower(r,NM(1.0/n))+1));
   }
   //List(f,r);
   List(f,QuotientN(m,PowerN(f,n))); //
}

// lookup table utilities
Procedure("NthRootList",["n"])
{
   Decide(Length(NthRootTable)>?0,
   {
      Local(p,xx);
      p:=Select(NthRootTable, '[[xx],First(xx)=?n]);
      Decide(Length(p)=?1,Rest(p[1]),List());
   },
   List());
}

Procedure("NthRootRestore",["m", "n"])
{
   Local(p);
   p:=NthRootList(n);
   Decide(Length(p)>?0,
   {
      Local(r,xx);
      r:=Select(p, '[[xx],First(xx)=?m]);
      Decide(Length(r)=?1,First(Rest(r[1])),List());
   },
   List());
}

Procedure("NthRootSave",["m", "n", "r"])
{
   Local(p);
   p:=NthRootList(n);
   Decide(Length(p)=?0,
   // create power list and save root
   Insert!(NthRootTable,1,List(n,List(m,r))),
   {
      Local(rr,xx);
      rr:=Select(p, '[[xx],First(xx)=?m]);
      Decide(Length(rr)=?0,
      {
         // save root only
         Append!(p,List(m,r));
      },
      // already saved
      False);
   });
}

//TODO why is NthRootTable both lazy global and protected with LocalSymbols?
Procedure("NthRootClear",[]) AssignGlobalLazy(NthRootTable,List());

// create empty table
NthRootClear();

} // LocalSymbols(m,n,r,NthRootTable);

//////
//////


%/mathpiper



%mathpiper_docs,name="NthRoot",categories="Mathematics Procedures,Numbers (Operations)"
*CMD NthRoot --- calculate/simplify nth root of an integer
*STD
*CALL
        NthRoot(m,n)

*PARMS

{m} -- a non-negative integer ($m>0$)

{n} -- a positive integer greater than 1 ($n>1$)

*DESC

{NthRoot(m,n)} calculates the integer part of the $n$-th root $m^{1/n}$ and
returns a list {[f,r]}. {f} and {r} are both positive integers
that satisfy $f^n*r$=$m$.
In other words, $f$ is the largest integer such that $m$ divides $f^n$ and 
$r$ is the remaining factor.

For large {m} and small {n}
{NthRoot} may work quite slowly. Every result {[f,r]} for given
{m}, {n} is saved in a lookup table, thus subsequent calls to
{NthRoot} with the same values {m}, {n} will be executed quite
fast.

*E.G.
In> NthRoot(12,2)
Result: [2,3];

In> NthRoot(81,3)
Result: [3,3];

In> NthRoot(3255552,2)
Result: [144,157];

In> NthRoot(3255552,3)
Result: [12,1884];

*SEE PowerN
%/mathpiper_docs

*SEE IntNthRoot, Factors




%mathpiper,name="NthRoot",subtype="automatic_test"

// you need to use ListToProcedure for this one as -1 is actually -(1), eg. a unary procedure (minus)
// applied to a positive integer (1). ListToProcedure evaluates its arguments, resulting in a negative
// integer (-1).
Verify(NthRoot(-1,2),ListToProcedure(['NthRoot,-1,2]));

Verify(NthRoot(2,1),Hold(NthRoot(2,1)));
Verify(NthRoot(2,2),[1,2]);
Verify(NthRoot(12,2),[2,3]);
Verify(NthRoot(12,3),[1,12]);
Verify(NthRoot(27,3),[3,1]);
Verify(NthRoot(17*13,2),[1,17*13]);
Verify(NthRoot(17*17*13,2),[17,13]);
Verify(NthRoot(17*17*17*13,2),[17,17*13]);
Verify(NthRoot(17*17*17*13,3),[17,13]);
Verify(NthRoot(17*17*17*17*13*13,2),[17*17*13,1]);
Verify(NthRoot(17*17*17*17*13*13,3),[17,17*13*13]);
Verify(NthRoot(17*17*17*17*13*13,4),[17,13*13]);
Verify(NthRoot(17*17*17*17*13*13,5),[1,17*17*17*17*13*13]);

%/mathpiper