%mathpiper,def="ReadLines"

Retract("ReadLines", All);

10 ## ReadLines(filename_String?) <--
{
    Check(FileExists?(filename), "The file <" + filename + "> does not exist.");

    Local(lines, file);
    
    file := FileReaderOpen(filename);
    
    lines := ReadLines(file);
    
    FileClose(file);
    
    lines;
}


20 ## ReadLines(reader_) <--
{
    Local(lines, line);
    
    lines := [];
    
    While((line := ReadLine(reader)) !=? False)
    {
        Append!(lines, line);
    }
    
    lines;
}

%/mathpiper





%mathpiper_docs,name="ReadLines",categories="Programming Procedures,Input/Output",access="experimental"
*CMD  ReadLines --- returns the lines of a text file as a list of strings
*CALL
        ReadLines(filename)
        ReadLines(reader)

*PARMS
{filename} -- a string that contains the name of a file

{reader} -- a reader

*DESC
This procedure returns the lines of a text file as a list of strings. The full path of the
file must be provided if the version of the procedure that accepts a filename is used.

*E.G.
ReadLines("testfile.txt")
%/mathpiper_docs