%mathpiper,def="StringReplace"

StringReplace(string, substring, replacement) :=
{
    Local(javaString);
    javaString := JavaNew("java.lang.String", string);
    JavaAccess(javaString,"replace", substring, replacement);
}

%/mathpiper





%mathpiper_docs,name="StringReplace",categories="Programming Procedures,Strings",access="experimental"
*CMD StringReplace --- replace substrings of a string with another string
*CALL
        StringReplace(string, substring, replacement)

*PARMS
{string} -- the parent string

{substring} -- the substring within the string to be replaced

{replacement} -- the string with which to replace all occurrences of the substring

*DESC
This procedure replaces all occurrences of a substring within a string with 
a replacement string and returns the new string. If the substring 
is not located within the string, then the procedures returns the 
string unaltered. This procedure is case sensitive.

*E.G.
In> StringReplace("Heljo", "j", "l")
Result: "Hello"

In> StringReplace("RedBlueRedGreen", "Green", "Blue")
Result: "RedBlueRedBlue"

In> StringReplace("Yes", "yes", "no")
Result: "Yes"
%/mathpiper_docs





%mathpiper,name="StringReplace",subtype="automatic_test"

Verify(StringReplace("Heljo", "j", "l"),"Hello" );
Verify(StringReplace("","","l"), "l");

%/mathpiper




