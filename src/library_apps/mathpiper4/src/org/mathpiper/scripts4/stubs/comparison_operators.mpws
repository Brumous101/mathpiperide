%mathpiper,def="<?;>?;<=?;>=?;!=?"

/* def file definitions
=
<?
>?
<=?
>=?
!=?

*/

/* Comparison operators. They call the internal comparison routines when
 * both arguments are numbers. The value Infinity is also understood.
*/

// Undefined is a very special case as we return False for everything
1 ## Undefined <?  x_  <--  False;
1 ## Undefined <=? x_  <--  False;
1 ## Undefined >?  x_  <--  False;
1 ## Undefined >=? x_  <--  False;
1 ## x_ <?  Undefined  <--  False;
1 ## x_ <=? Undefined  <--  False;
1 ## x_ >?  Undefined  <--  False;
1 ## x_ >=? Undefined  <--  False;


// If n and m are numbers, use the standard LessThan procedure immediately
5 ## (n_Number? <? m_Number?) <-- LessThan?(n-m,0);


// If n and m are symbolic after a single evaluation, see if they can be coerced in to a real-valued number.
LocalSymbols(nNum,mNum)
{
  10 ## (n_ <? m_)::{nNum:=NM(Eval(n)); mNum:=NM(Eval(m));Number?(nNum) &? Number?(mNum);} <-- LessThan?(nNum-mNum,0);
}

// Deal with Infinity
20 ##  (Infinity <? n_)::(Not?(Infinity?(n)))  <-- False;
20 ##  (-Infinity <? n_)::(Not?(Infinity?(n))) <-- True;
20 ##  (n_ <? Infinity)::(Not?(Infinity?(n)))  <-- True;
20 ##  (n_ <? -Infinity)::(Not?(Infinity?(n))) <-- False;

// Lots of known identities go here
30 ## (n1_/n2_) <? 0  <--  (n1 <? 0) !=? (n2 <? 0);
30 ## (n1_*n2_) <? 0  <--  (n1 <? 0) !=? (n2 <? 0);

// This doesn't sadly cover the case where a and b have opposite signs
30 ## ((n1_+n2_) <? 0)::((n1 <? 0) &? (n2 <? 0))  <--  True;
30 ## ((n1_+n2v) <? 0)::((n1 >? 0) &? (n2 >? 0))  <--  False;
30 ##  x_^a_Odd?  <? 0  <--  x <? 0;
30 ##  x_^a_Even? <? 0  <--  False; // This is wrong for complex x

// Add other procedures here!  Everything we can compare to 0 should be here.
40 ## ((Sqrt(x_))::(x >? 0)) <? 0          <--  False;

//40 ## (Sin(x_) <? 0)::(Not?(Even?(NM(x/Pi))) &? Even?(NM(Floor(x/Pi)))) <-- False;
//40 ## (Sin(x_) <? 0)::(Not?(Odd? (NM(x/Pi))) &? Odd? (NM(Floor(x/Pi)))) <-- True;

//40 ## Cos(x_) <? 0 <-- Sin(Pi/2-x) <? 0;

//40 ## (Tan(x_) <? 0)::(Not?(Even?(NM(2*x/Pi))) &? Even?(NM(Floor(2*x/Pi)))) <-- False;
//40 ## (Tan(x_) <? 0)::(Not?(Odd? (NM(2*x/Pi))) &? Odd? (NM(Floor(2*x/Pi)))) <-- True;

// Functions that need special treatment with more than one of the comparison
// operators as they always return true or false.  For these we must define
// both the `<?' and `>=?' operators.
40 ## (Complex(a_,b_) <?  0)::(b!=?0) <--  False;
40 ## (Complex(a_,b_) >=? 0)::(b!=?0) <--  False;
40 ## ((Sqrt(x_))::(x <? 0)) <?  0      <--  False;
40 ## ((Sqrt(x_))::(x <? 0)) >=? 0      <--  False;

// Deal with negated terms
43 ## -(x_) <? 0 <-- Not?((x<?0) |? (x=?0));


// Handle string comparisons.
45 ## (n_String? <? m_String?) <-- LessThan?(n, m);


// Define each of [>?,<=?,>=?] in terms of <?
50 ## n_ >?  m_ <-- m <? n;
50 ## n_ <=? m_ <-- m >=? n;
50 ## n_ >=? m_ <-- Not?(n<?m);


Procedure("!=?",["aLeft", "aRight"]) Not?(aLeft=?aRight);

%/mathpiper



%mathpiper_docs,name="<?",categories="Operators"
*CMD <? --- test for "less than"
*STD
*CALL
        e1 <? e2

*PARMS

{e1}, {e2} -- expressions to be compared

*DESC

The two expression are evaluated. If both results are numeric, they
are compared. If the first expression is smaller than the second one,
the result is {True} and it is {False} otherwise. If either of the expression is not numeric, after
evaluation, the expression is returned with evaluated arguments.

The word "numeric" in the previous paragraph has the following
meaning. An expression is numeric if it is either a number (i.e. {Number?} returns {True}), or the
quotient of two numbers, or an infinity (i.e. {Infinity?} returns {True}). MathPiper will try to 
coerce the arguments passed to this comparison operator to a real value before making the comparison.

*E.G.

In> 2 <? 5;
Result: True;

In> Cos(1) <? 5;
Result: True;

*SEE Number?, Infinity?, NM
%/mathpiper_docs



%mathpiper_docs,name=">?",categories="Operators"
*CMD >? --- test for "greater than"
*STD
*CALL
        e1 >? e2

*PARMS

{e1}, {e2} -- expressions to be compared

*DESC

The two expression are evaluated. If both results are numeric, they
are compared. If the first expression is larger than the second one,
the result is {True} and it is {False} otherwise. If either of the expression is not numeric, after
evaluation, the expression is returned with evaluated arguments.

The word "numeric" in the previous paragraph has the following
meaning. An expression is numeric if it is either a number (i.e. {Number?} returns {True}), or the
quotient of two numbers, or an infinity (i.e. {Infinity?} returns {True}). MathPiper will try to 
coerce the arguments passed to this comparison operator to a real value before making the comparison.

*E.G.

In> 2 >? 5;
Result: False;

In> Cos(1) >? 5;
Result: False

*SEE Number?, Infinity?, NM
%/mathpiper_docs



%mathpiper_docs,name="<=?",categories="Operators"
*CMD <=? --- test for "less or equal"
*STD
*CALL
        e1 <=? e2

*PARMS

{e1}, {e2} -- expressions to be compared

*DESC

The two expression are evaluated. If both results are numeric, they
are compared. If the first expression is smaller than or equals the
second one, the result is {True} and it is {False} otherwise. If either of the expression is not
numeric, after evaluation, the expression is returned with evaluated
arguments.

The word "numeric" in the previous paragraph has the following
meaning. An expression is numeric if it is either a number (i.e. {Number?} returns {True}), or the
quotient of two numbers, or an infinity (i.e. {Infinity?} returns {True}). MathPiper will try to 
coerce the arguments passed to this comparison operator to a real value before making the comparison.

*E.G.

In> 2 <=? 5;
Result: True;

In> Cos(1) <=? 5;
Result: True

*SEE Number?, Infinity?, NM
%/mathpiper_docs



%mathpiper_docs,name=">=?",categories="Operators"
*CMD >=? --- test for "greater or equal"
*STD
*CALL
        e1 >=? e2

*PARMS

{e1}, {e2} -- expressions to be compared

*DESC

The two expression are evaluated. If both results are numeric, they
are compared. If the first expression is larger than or equals the
second one, the result is {True} and it is {False} otherwise. If either of the expression is not
numeric, after evaluation, the expression is returned with evaluated
arguments.

The word "numeric" in the previous paragraph has the following
meaning. An expression is numeric if it is either a number (i.e. {Number?} returns {True}), or the
quotient of two numbers, or an infinity (i.e. {Infinity?} returns {True}). MathPiper will try to 
coerce the arguments passed to this comparison operator to a real value before making the comparison.

*E.G.

In> 2 >=? 5;
Result: False;

In> Cos(1) >=? 5;
Result: False

*SEE Number?, Infinity?, NM
%/mathpiper_docs



%mathpiper_docs,name="!=?",categories="Operators"
*CMD !=? --- test for "not equal"
*STD
*CALL
        e1 !=? e2

*PARMS

{e1}, {e2} -- expressions to be compared

*DESC

Both expressions are evaluated and compared. If they turn out to be
equal, the result is {False}. Otherwise, the result
is {True}.

The expression {e1 !=? e2} is equivalent to {Not?(e1 = e2)}.

*E.G.

In> 1 !=? 2;
Result: True;

In> 1 !=? 1;
Result: False;

*SEE =?
%/mathpiper_docs



%mathpiper_docs,name="=?",categories="Operators"
*CMD = --- test for equality of expressions
*STD
*CALL
        e1 =? e2

*PARMS

{e1}, {e2} -- expressions to be compared

*DESC

Both expressions are evaluated and compared. If they turn out to be equal, the
result is {True}. Otherwise, the result is {False}. The procedure {Equals?} does
the same.

Note that the test is on syntactic equality, not mathematical equality. Hence
even if the result is {False}, the expressions can still be
<i>mathematically</i> equal; see the examples below. Put otherwise, this
procedure tests whether the two expressions would be displayed in the same way
if they were printed.

*E.G.

In> e1 := '((x+1) * (x-1));
Result: (x+1)*(x-1);

In> e2 := '(x^2 - 1);
Result: x^2-1;
        

In> e1 =? e2;
Result: False;


*SEE !=?, Equals?
%/mathpiper_docs





%mathpiper,name="<?",subtype="automatic_test"

Verify(.1<?2,True);
Verify(0.1<?2,True);
Verify(.3<?2,True);
Verify(2<?.1,False);
Verify(2<?0.1,False);
Verify(2<?.3,False);
Verify(1e-5 <? 1, True);
Verify(1e-5 <? 2e-5, True);
Verify(1e-1 <? 2e-1, True);
Verify(1e-15 <? 2e-15, True);
Verify(1e-5 <? 1e-10, False);
Verify(1e-5 <? 1e-2, True);
Verify(-1e-5 <? 1e-5, True);
Verify(-1e-5 <? 1e-6, True);

BuiltinPrecisionSet(32);
Verify(0.999999999999999999999999999992 <? 1, True);
BuiltinPrecisionSet(10);

Verify(_a<?0,_a<?0);

Verify(Undefined<?1, False);

Verify(500 <? 0e-1,False);

//Bug reported by Adrian Vontobel: comparison operators should coerce
//to a real value as much as possible before trying the comparison.
Verify(0.2*Pi <? 0.7, True);
Verify(0.2*Pi <? 0.6, False);

Verify("a" <? "b", True);

%/mathpiper





%mathpiper,name=">?",subtype="automatic_test"

Verify(.1>?2,False);
Verify(0.1>?2,False);
Verify(.3>?2,False);
Verify(2>?.1,True);
Verify(2>?0.1,True);
Verify(2>?.3,True);
Verify(1e-15 >? 0, True);
Verify(1e-5 >? 0, True);
Verify(1e-4 >? 0, True);
Verify(1e-3 >? 0, True);
Verify(1e-2 >? 0, True);
Verify(1e-1 >? 0, True);
Verify(1e5 >? 0, True);

BuiltinPrecisionSet(32);
Verify(1.0000000000000000000000000000111 >? 1, True);
BuiltinPrecisionSet(10);

Verify(_a>?0,_a>?0);

Verify(Undefined>?Undefined, False);
Verify(Undefined>?1, False);

//Bug reported by Adrian Vontobel: comparison operators should coerce
//to a real value as much as possible before trying the comparison.
Verify(0.2*Pi >? 0.5, True);
Verify(0.2*Pi >? 0.7, False);

%/mathpiper





%mathpiper,name="=?",subtype="automatic_test"

Verify(1e-5 =? 2e-5, False);
Verify(1e-5 =? 1e-6, False);

%/mathpiper





%mathpiper,name="!=?",subtype="automatic_test"

Verify((1==1) !=? True, True);
Verify((_a==_a) !=? True, True);
Verify((1==2) !=? False, True);
Verify((_a==2) !=? False, True);

%/mathpiper






%mathpiper,name=">=?",subtype="automatic_test"

Verify( 2/3 >=? 1/3, True);
Verify(1 >=? 1.0, True);
Verify(-1 >=? -1.0, True);
Verify(0 >=? 0.0, True);
Verify(0.0 >=? 0, True);
Verify(Undefined >=? -4, False);

%/mathpiper





%mathpiper,name="<=?",subtype="automatic_test"

Verify(1 <=? 1.0, True);
Verify(-1 <=? -1.0, True);
Verify(0 <=? 0.0, True);
Verify(0.0 <=? 0, True);
Verify(Undefined <=? -4, False);

%/mathpiper