%mathpiper,def="UnparseActivityDiagram;UnparseActivityDiagramPrivate;UnparseActivityDiagramPrivateDoublePrecisionNumber;UnparseActivityDiagramPrivateArgs;UnparseActivityDiagramPrivateStatement;UnparseActivityDiagramPrivateMaxPrec;UnparseActivityDiagramPrivateRegularOpsUnparseActivityDiagramPrivateMathFunctions;NlActivityIndented;ActivityIndented;ActivityUnindent",indent="false"

/*
Retract("UnparseActivityDiagramPrivate", All);
Retract("UnparseActivityDiagramPrivateBracketIf", All);
Retract("UnparseActivityDiagramPrivateDoublePrecisionNumber", All);
Retract("UnparseRegularOps", All);
Retract("UnparseMathFunctions", All);
Retract("UnparseActivityDiagramPrivateArgs", All);
Retract("UnparseActivityDiagramPrivateMathFunctions", All);
Retract("UnparseActivityDiagramPrivateMaxPrec", All);
Retract("UnparseActivityDiagramPrivateStatement", All);
*/


Constant('NO?ENDIF, True);
Constant('NO?SPACES, True);

RulebaseHoldArguments("UnparseActivityDiagramPrivate",["expression", "metaInfo"]);
RulebaseHoldArguments("UnparseActivityDiagramPrivate",["expression", "precedence", "metaInfo"]);

Procedure("UnparseActivityDiagramPrivateBracketIf", ["predicate", "string"])
{
    Check(Boolean?(predicate) &? String?(string), "UnparseActivityDiagramPrivate internal error: non-boolean and/or non-string argument of UnparseActivityDiagramPrivateBracketIf");
    
    Decide(predicate, ConcatStrings("( ", string, ") "), string);
}

UnparseActivityDiagramPrivateDoublePrecisionNumber(x_Number?) <--
{
    Local(i,n,s,f);
    s := ToString(x);
    n := Length(s);
    f := False;
    For(i := 1, i <=? n, i++)
    {
        Decide(s[i] =? "e" |? s[i] =? ".", f := True);
    }
    Decide(f, s, s + ".");
}

/* Proceed just like UnparseLatex()
*/

// UnparseActivityDiagramPrivateMaxPrec should perhaps only be used from within this file, it is thus not in the .def file.
UnparseActivityDiagramPrivateMaxPrec() := 60000;         /* This precedence will never be bracketed. It is equal to KMaxPrec */


// Main.
UnparseActivityDiagram(x) :=
{
    UnparseActivityDiagram(x, 3);
}

UnparseActivityDiagram(x, scale) := "@startuml" + Nl() + "start" + Nl() +"scale " + scale + Nl() + "skinparam shadowing false" + Nl() + Nl() + UnparseActivityDiagramPrivate(x, None) + Nl() + Nl() + "stop" + Nl() + "@enduml";


// Root.
100 ## UnparseActivityDiagramPrivate(x_, metaInfo_) <-- UnparseActivityDiagramPrivate(x, UnparseActivityDiagramPrivateMaxPrec(), None);


/* Replace numbers and variables -- never bracketed except explicitly */
110 ## UnparseActivityDiagramPrivate(x_Integer?, p_, metaInfo_) <-- ToString(x);
111 ## UnparseActivityDiagramPrivate(x_Zero?, p_, metaInfo_) <-- "0.";
112 ## UnparseActivityDiagramPrivate(x_Number?, p_, metaInfo_) <-- UnparseActivityDiagramPrivateDoublePrecisionNumber(x);


/* Variables are left as is, except some special ones */
190 ## UnparseActivityDiagramPrivate(False, p_, metaInfo_) <-- "False";
190 ## UnparseActivityDiagramPrivate(True, p_, metaInfo_) <-- "True";
200 ## UnparseActivityDiagramPrivate(x_Atom?, p_, metaInfo_) <-- ToString(x);


/* Strings must be quoted but not bracketed */
100 ## UnparseActivityDiagramPrivate(x_String?, p_, metaInfo_) <-- ConcatStrings("\"", x, "\"");

/* Replace operations:arithmetic
addition, subtraction, multiplication, all comparison and logical operations are "regular" */


LocalSymbols(activityUnparserRegularOps) {
  activityUnparserRegularOps := [ ["+"," + "], ["-"," - "], ["*"," * "],
                       ["/"," / "], [":="," := "], ["=="," == "],
                       ["=?"," =? "], ["!=?"," !=? "], ["<=?"," <=? "],
                       [">=?"," >=? "], ["<?"," <? "], [">?"," >? "],
                       ["&?"," &? "], ["|?"," |? "], [">>", " >> "],
                       [ "<<", " << " ],
                       [ "%", " % " ], [ "^", " ^ " ],
                     ];

  UnparseActivityDiagramPrivateRegularOps() := activityUnparserRegularOps;
} // LocalSymbols(activityUnparserRegularOps)



        /* This is the template for "regular" binary infix operators:
100 ## UnparseActivityDiagramPrivate(x_ + y_, p_, metaInfo_) <-- UnparseActivityDiagramPrivateBracketIf(p <? PrecedenceGet("+"), ConcatStrings(UnparseActivityDiagramPrivate(x, LeftPrecedenceGet("+")), " + ", UnparseActivityDiagramPrivate(y, RightPrecedenceGet("+")) ) );
        

        
        /* unary addition */
100 ## UnparseActivityDiagramPrivate(+ y_, p_, metaInfo_) <-- UnparseActivityDiagramPrivateBracketIf(p <? PrecedenceGet("+"), ConcatStrings(" + ", UnparseActivityDiagramPrivate(y, RightPrecedenceGet("+"), None) ) );


        /* unary subtraction */
100 ## UnparseActivityDiagramPrivate(- y_, p_, metaInfo_) <-- UnparseActivityDiagramPrivateBracketIf(p <? PrecedenceGet("-"), ConcatStrings(" - ", UnparseActivityDiagramPrivate(y, RightPrecedenceGet("-"), None) ) );


        /* power's argument is never bracketed but it must be put in braces. */
100 ## UnparseActivityDiagramPrivate(x_ ^ y_, p_, metaInfo_) <-- UnparseActivityDiagramPrivateBracketIf(p <=? PrecedenceGet("^"), ConcatStrings("pow(", UnparseActivityDiagramPrivate(x, UnparseActivityDiagramPrivateMaxPrec(), None), ", ", UnparseActivityDiagramPrivate(y, UnparseActivityDiagramPrivateMaxPrec(), None), ")" ) );




        /* If */
        
107 ## UnparseActivityDiagramPrivate(If(pred_) body_, p_, metaInfo_)::(Type(body) =? "Sequence" ) <-- "if(" + UnparseActivityDiagramPrivate(pred, 60000, False) + ") " + "then (True)"  + UnparseActivityDiagramPrivate(body, None) + Decide(metaInfo =? NO?ENDIF, "", "endif");

108 ## UnparseActivityDiagramPrivate(If(pred_) body_, p_, metaInfo_) <-- "if(" + UnparseActivityDiagramPrivate(pred, 60000, False) + ") " + "then (True)" + Nl() + ":" + UnparseActivityDiagramPrivate(body, None) + ";" + Nl() +"endif";

        /* Else */
103 ## UnparseActivityDiagramPrivate(left_ Else right_, p_, metaInfo_)::(Type(left) =? "If" &? Type(right) =? "Else") <-- "if(" + UnparseActivityDiagramPrivate(left[1], 60000, False) + ") " + "then (True)" + UnparseActivityDiagramPrivate(left[2], None) + "else" + UnparseActivityDiagramPrivate(right, NO?ENDIF);

104 ## UnparseActivityDiagramPrivate(left_ Else right_, p_, metaInfo_)::(Type(left) =? "If" &? Type(right) !=? "Else") <-- "if(" + UnparseActivityDiagramPrivate(left[1], 60000, False) + ") " + "then (True)" + UnparseActivityDiagramPrivate(left[2], None) + "else" + UnparseActivityDiagramPrivate(right, None) + "endif";

// 105 ## UnparseActivityDiagramPrivate(left_ Else right_, p_, metaInfo_) <-- UnparseActivityDiagramPrivate(left, 60000, False) + "else3" + UnparseActivityDiagramPrivate(right, False);


LocalSymbols(cUnparserMathFunctions) {
  cUnparserMathFunctions :=
    [
      ["Sqrt","sqrt"],
      ["Cos","cos"],
      ["Sin","sin"],
      ["Tan","tan"],
      ["Cosh","cosh"],
      ["Sinh","sinh"],
      ["Tanh","tanh"],
      ["Exp","exp"],
      ["Ln","log"],
      ["ArcCos","acos"],
      ["ArcSin","asin"],
      ["ArcTan","atan"],
      ["ArcCosh","acosh"],
      ["ArcSinh","asinh"],
      ["ArcTanh","atanh"],
      ["Maximum","max"],
      ["Minimum","min"],
      ["Abs","fabs"],
      ["Floor","floor"],
      ["Ceil","ceil"]
    ];

  UnparseActivityDiagramPrivateMathFunctions() := cUnparserMathFunctions;

} // LocalSymbols(cUnparserMathFunctions)


// Regular operators.
/* Precedence of 120 because we'd like to process some special procedures like pow() first */
120 ## UnparseActivityDiagramPrivate(expr_Procedure?, p_, metaInfo_)::(ArgumentsCount(expr) =? 2 &? Contains?(AssociationIndices(UnparseActivityDiagramPrivateRegularOps()), Type(expr))) <--
      UnparseActivityDiagramPrivateBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseActivityDiagramPrivate(ProcedureToList(expr)[2], LeftPrecedenceGet(Type(expr)), None), UnparseActivityDiagramPrivateRegularOps()[Type(expr)], UnparseActivityDiagramPrivate(ProcedureToList(expr)[3], RightPrecedenceGet(Type(expr)), None) ) );


/* Sin, Cos, etc. and their argument is always bracketed */
120 ## UnparseActivityDiagramPrivate(expr_Procedure?, p_, metaInfo_) ::
      (ArgumentsCount(expr) =? 1 &? Contains?(AssociationIndices(UnparseActivityDiagramPrivateMathFunctions()), Type(expr))) <--
      ConcatStrings(UnparseActivityDiagramPrivateMathFunctions()[Type(expr)], "(", UnparseActivityDiagramPrivate( ProcedureToList(expr)[2], UnparseActivityDiagramPrivateMaxPrec(), None),")" );

      
/* procedures */
/* Unknown procedure, precedence 200. Leave as is, never bracket the procedure itself 
and bracket the argumentPointer(s) automatically since it's a list. Other procedures are precedence 100 */
UnparseActivityDiagramPrivateArgs(list_List?, metaInfo_) <--
{
  Local(i, nr, result);
  result := "";
  nr := Length(list);
  For (i := 1,i <=? nr,i++)
  {
    result := result + UnparseActivityDiagramPrivate(list[i], None);
    Decide(i <? nr, result := result + Decide(metaInfo =? NO?SPACES, ",", ", "));
  }
  result;
}


/* List literals */
198 ## UnparseActivityDiagramPrivate(x_, p_, metaInfo_)::(Procedure?(x) &? Type(x) =? "List") <--
{
  ConcatStrings( "[", UnparseActivityDiagramPrivateArgs(Rest(ProcedureToList(x)), NO?SPACES),"]" );
}


/* Procedures */
200 ## UnparseActivityDiagramPrivate(x_, p_, metaInfo_)::(Procedure?(x)) <--
{
  ConcatStrings(Type(x), "(", UnparseActivityDiagramPrivateArgs(Rest(ProcedureToList(x)), None),")" );
}


/* Complex numbers */
100 ## UnparseActivityDiagramPrivate(Complex(0, 1), p_, metaInfo_) <-- "I";
100 ## UnparseActivityDiagramPrivate(Complex(x_, 0), p_, metaInfo_) <-- UnparseActivityDiagramPrivate(x, p, None);
110 ## UnparseActivityDiagramPrivate(Complex(x_, 1), p_, metaInfo_) <-- UnparseActivityDiagramPrivate(x+Hold(I), p, None);
110 ## UnparseActivityDiagramPrivate(Complex(0, y_), p_, metaInfo_) <-- UnparseActivityDiagramPrivate(Hold(I)*y, p, None);
120 ## UnparseActivityDiagramPrivate(Complex(x_, y_), p_, metaInfo_) <-- UnparseActivityDiagramPrivate(x+Hold(I)*y, p, None);

/* Some special procedures: Mod */
100 ## UnparseActivityDiagramPrivate(Modulo(x_, y_), p_, metaInfo_) <-- UnparseActivityDiagramPrivateBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseActivityDiagramPrivate(x, PrecedenceGet("/"), None), " % ", UnparseActivityDiagramPrivate(y, PrecedenceGet("/"), None) ) )
;

/* Indexed expressions are never bracketed */
// the rule with [ ] seems to have no effect?
//100 ## UnparseActivityDiagramPrivate(x_ [ i_ ], p_, metaInfo_) <-- ConcatStrings(UnparseActivityDiagramPrivate(x, UnparseActivityDiagramPrivateMaxPrec(), None), "[", UnparseActivityDiagramPrivate(i, UnparseActivityDiagramPrivateMaxPrec(), None), "]");
100 ## UnparseActivityDiagramPrivate(Nth(x_, i_), p_, metaInfo_) <-- ConcatStrings(UnparseActivityDiagramPrivate(x, UnparseActivityDiagramPrivateMaxPrec(), None), "[", UnparseActivityDiagramPrivate(i, UnparseActivityDiagramPrivateMaxPrec(), None), "]");

LocalSymbols(activityIndent) {
  activityIndent := 1;

  NlActivityIndented() := 
  {
    Local(result, i);
    // carriage return, so needs to start at the beginning of the line
    result := Nl();

    For(i := 1, i <? activityIndent, i++)
    {
      result := result + "  ";
    }
    
    result;
  }
  
  ActivityIndent() :=
  {
      activityIndent++;
      "";
  }
  
  
  ActivityUnindent() :=
  {
      activityIndent--;
      "";
  }
  
} // LocalSymbols(activityIndent)



UnparseActivityDiagramPrivateStatement(x_, metaInfo_) <-- Decide(Contains?(["If", "Else", "While", "For", "Sequence", "Until"], Type(x)), "", ":") + UnparseActivityDiagramPrivate(x, None) + Decide(Contains?(["If","Else", "While", "For", "Sequence", "Until"], Type(x)), "", ";") + NlActivityIndented();

120 ## UnparseActivityDiagramPrivate(x_, p_, metaInfo_)::(Type(x) =? "Sequence") <--
{
  Local(result);
  
  result := ActivityIndent() + NlActivityIndented();
  
  ForEach(item,Rest(ProcedureToList(x)))
  {
    result := result + UnparseActivityDiagramPrivateStatement(item, None);
  }
  
  result := result + ActivityUnindent() + NlActivityIndented();
  
  result;
}


/*
120 ## UnparseActivityDiagramPrivate(For(_from,_to,_step)_body,_p, metaInfo_) <--
  "for(" + UnparseActivityDiagramPrivate(from,UnparseActivityDiagramPrivateMaxPrec(), None) + ";"
        + UnparseActivityDiagramPrivate(to,UnparseActivityDiagramPrivateMaxPrec(), None) + ";"
        + UnparseActivityDiagramPrivate(step,UnparseActivityDiagramPrivateMaxPrec(), None) + ")"
        + ActivityIndent() + NlActivityIndented()
        + UnparseActivityDiagramPrivateStatement(body, None) + ActivityUnindent();  

repeat
  :read data;
  :generate diagrams;
repeat while (more data?)
 */      
      
        


120 ## UnparseActivityDiagramPrivate(For(from_,to_,step_) body_, p_, metaInfo_) <--
    UnparseActivityDiagramPrivateStatement(from, None) + NlActivityIndented() +
      "repeat" + ActivityIndent() + NlActivityIndented() +
      UnparseActivityDiagramPrivateStatement(body, None) +
      UnparseActivityDiagramPrivateStatement(step, None) +
      ActivityUnindent() + NlActivityIndented() +"repeat while (" + UnparseActivityDiagramPrivate(to, UnparseActivityDiagramPrivateMaxPrec(), None) + ") is (True)" + ActivityUnindent();
        
        

120 ## UnparseActivityDiagramPrivate(While(pred_) body_, p_, metaInfo_) <--
        "while(" + UnparseActivityDiagramPrivate(pred,UnparseActivityDiagramPrivateMaxPrec(), False) + ") is (True)"
        + ActivityIndent() + NlActivityIndented()
        + UnparseActivityDiagramPrivateStatement(body, None) + ActivityUnindent() + "endwhile (False)";

        
120 ## UnparseActivityDiagramPrivate(Until(pred_) body_, p_, metaInfo_) <--
      "repeat" + ActivityIndent() + NlActivityIndented() +
      UnparseActivityDiagramPrivateStatement(body, None) +
      ActivityUnindent() + NlActivityIndented() +"repeat while (" + UnparseActivityDiagramPrivate(pred, UnparseActivityDiagramPrivateMaxPrec(), None) + ") is (False)" + ActivityUnindent();
        
        
       

%/mathpiper


        


%mathpiper,indent="false",output="plantuml"

aa := '(
{
2+2; 
3+3;
x := 7;
If(x <=? 3)
{
    Echo("True");
}
Else
{
    Echo("False");
}

});

bb := '(
{
2+2; 
3+3;
x := 7;
If(x <=? 3)
{
    Echo("True");
}

});

cc := '(
{
x := 7;
If(x <=? 3)
{
    Echo("True");
}
Else If(x >=? 8)
{
    Echo("Maybe");
}

});


dd := '{
x := 1;
While(x <? 50)
{
  Write(x);
  WriteString(",");
  x := x + 1;  
}
}

UnparseActivityDiagramPrivate(dd);


%/mathpiper

    %plantuml,sequence="88",timestamp="2016-07-19 15:50:35.504",preserve="false"
Result: 
"@startuml
start
scale 2.0
skinparam shadowing false


  :aa := '(
    :2 + 2;
    :3 + 3;
    :x := 7;
    if4(x <=? 3) then (True)
      :Echo("True");
      
    else2
      :Echo("False");
      
    endif3
    
  );
  :bb := '(
    :2 + 2;
    :3 + 3;
    :x := 7;
    if1(x <=? 3) then (True)
      :Echo("True");
      
    endif1
    
  );
  :cc := '(
    :x := 7;
    if3(x <=? 3) then (True)
      :Echo("True");
      
    else1if1(x >=? 8) then (True)
      :Echo("Maybe");
      
    endif1
    
  );
  :dd := '(
    :x := 1;
    while(x <? 50) is (True)
      
        :Write(x);
        :WriteString(",");
        :x := x + 1;
        
      
      endwhile (False)
    
  );
  :UnparseActivityDiagramPrivate(dd);
  


stop
@enduml"
.   %/plantuml

    %output,sequence="9",timestamp="2015-03-30 13:51:46.873",preserve="true"
Result: 
"@startuml
start
scale 2.0
skinparam shadowing false


  :x := 1;
  while(x < 50) is (True)
    
      :Write(x);
      :WriteString(",");
      :x := x + 1;
      
    
    endwhile (False)
  


stop
@enduml"
.   %/output





%plantuml,title=""

@startuml
start
scale 2.0
skinparam shadowing false

    :x := 1;
    while(x < 50) is (True)
      :Write(x);
      :WriteString(",");
      :x := x + 1;
      
      endwhile (False)
    
stop
@enduml

%/plantuml

    %output,sequence="7",timestamp="2015-03-08 16:08:23.734",preserve="false"
      
.   %/output





%mathpiper,title=""

code :=

'For(x := 1, x <=? 5, x := x + 1)
{
    Echo(x);
}


UnparseActivityDiagramPrivate(code);

%/mathpiper

    %error,sequence="44",timestamp="2015-03-22 12:05:24.676",preserve="false"
      Result: The procedure <UnparseActivityDiagramPrivateStatement> is not declared.  Error in another fold on or before line 264 starting at index 1.
.   %/error




