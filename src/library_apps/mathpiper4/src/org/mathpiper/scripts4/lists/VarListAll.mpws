%mathpiper,def="VarListAll"


VarListAll(expression) :=
{
    VarListAll(expression, []);
}


VarListAll(expression, list) :=
{   
    Local(index, listLength, nth);
    
    If(Atom?(expression))
    {
            If(Variable?(expression))
            {
            Append!(list, expression);
        }

    }
    Else If(Type(expression) =? "Nth")
    {
        nth := ProcedureToList(expression);
        
        VarListAll(nth[1]);
        
        VarListAll(nth[2]);
    }
    Else
    {    
        index := 0;
        
        listLength := Length(expression);
        
        While(index <=? listLength)
        {
            VarListAll(expression[index], list);
            
            index++;
        }
    }
        
    list;
}

%/mathpiper




%mathpiper_docs,name="VarListAll",categories="Programming Procedures,Lists (Operations)"
*CMD VarListAll --- list of variables appearing in an expression (with duplicates)
*STD
*CALL
        VarListAll(expr)

*PARMS

{expr} -- an expression

{list} -- a list of procedure atoms

*DESC

The command {VarListAll(expr)} returns a list of all variables (including duplicates) 
that appear in the expression {expr}. The expression is traversed recursively.

Note that since the operators "{+}" and "{-}" are prefix as well as infix operators, 
it is currently required to use {ToAtom("+")} to obtain the unevaluated atom "{+}".

*E.G.

In> VarListAll(x^2 + 2*x + x + a)
Result: [x,x,x,a]

*SEE VarList, VarListArith, VarListSome, FreeOf?, Variable?, ProcedureList, HasExpression?, HasProcedure?
%/mathpiper_docs