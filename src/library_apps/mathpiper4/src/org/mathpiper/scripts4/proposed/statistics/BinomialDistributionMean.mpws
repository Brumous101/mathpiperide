%mathpiper,def="BinomialDistributionMean"

BinomialDistributionMean(probability,numberOfTrials) :=
{

    Check(RationalOrNumber?(probability) &? p >=? 0 &? p <=? 1, "The first argument must be a number between 0 and 1.");
    
    Check(Integer?(numberOfTrials) &? numberOfTrials >=? 0, "The second argument must be an integer which is greater than 0.");
    
    numberOfTrials * probability;
}
        

%/mathpiper





%mathpiper_docs,name="BinomialDistributionMean",categories="Mathematics Procedures,Statistics & Probability",access="experimental"
*CMD BinomialDistributionMean --- the mean of a binomial distribution
*STD
*CALL
        BinomialDistributionMean(p,n)

*PARMS
{p} -- number, the probability of a success in a single trial

{n} -- number of trials

*DESC
This procedure calculates the mean of a binomial distribution. 

*E.G.
In> BinomialDistributionMean(.3,5)
Result: 1.5

*SEE BinomialDistributionStandardDeviation
%/mathpiper_docs



