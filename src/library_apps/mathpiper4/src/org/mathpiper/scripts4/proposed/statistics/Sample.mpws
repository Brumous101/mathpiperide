%mathpiper,def="Sample"

Sample(list, sampleSize) :=
{
    Check(List?(list), "The first argument must be a list.");
    
    Check(Integer?(sampleSize) &? sampleSize >? 0, "The second argument must be an integer which is greater than 0.");
    
    list := Shuffle(list);

    Take(list, sampleSize);
}

%/mathpiper





%mathpiper_docs,name="Sample",categories="Mathematics Procedures,Statistics & Probability",access="experimental"
*CMD Sample --- takes a random sample of elements from a list
*STD
*CALL
        Sample(list,sampleSize)

*PARMS
{list} -- a list of elements
{sampleSize} -- the size of the sample to take from the list

*DESC
This procedure takes a random sample of items from a list and returns
a list which contains the sample.

*E.G.

In> Sample([_a,_b,_c,_d,_e,_f,_g],3)
Result: [_a,_c,_g]

%/mathpiper_docs



