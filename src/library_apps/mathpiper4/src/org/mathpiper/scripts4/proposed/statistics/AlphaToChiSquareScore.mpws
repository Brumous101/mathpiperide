%mathpiper,def="AlphaToChiSquareScore",access="experimental"

/* 
    This procedure was adapted from the Javascript version of function
    that is located here:
    
    http://www.fourmilab.ch/rpkp/experiments/analysis/chiCalc.js
    http://www.fourmilab.ch/rpkp/experiments/analysis/chiCalc.html
    
    The following JavaScript procedures for calculating normal and
    chi-square probabilities and critical values were adapted by
    John Walker from C implementations
    written by Gary Perlman of Wang Institute, Tyngsboro, MA
    01879. Both the original C code and this JavaScript edition
    are in the public domain. 
*/



/*  CRITCHI  --  Compute critical chi-square value to
                     produce given p.  We just do a bisection
                     search for a value within CHI_EPSILON,
                     relying on the monotonicity of pochisq().  
*/


AlphaToChiSquareScore(p, df) :=
{
    Local(ChiEpsilon, ChiMax, minchisq, maxchisq, chisqval, result);
    
    ChiEpsilon := 0.000001;   /* Accuracy of critchi approximation */
    
    ChiMax := 99999.0;        /* Maximum chi-square value */
    
    minchisq := 0.0;
    
    maxchisq := ChiMax;
    
    p := NM(p);
    
    If( p <=? 0.0 |? p >=? 1.0)
    {
    
        If(p <=? 0.0) 
        {
            result := maxchisq;
        } 
        Else 
        {
            If(p >=? 1.0) 
            {
                result := 0.0;
            }
        }
    
    }
    Else
    {
        chisqval := NM(df / SqrtN(p));
        
        /* fair first value */
        While ((maxchisq - minchisq) >? ChiEpsilon) 
        {
            If(ChiSquareScoreToAlpha(chisqval, df) <? p) 
            {
                maxchisq := chisqval;
            } 
            Else 
            {
                minchisq := chisqval;
            }
            chisqval := (maxchisq + minchisq) * 0.5;
        }
        
        result := chisqval;
    
    }
    
    NM(result);
}


%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output






%mathpiper_docs,name="AlphaToChiSquareScore",categories="Mathematics Procedures,Statistics & Probability",access="experimental"
*CMD AlphaToChiSquareScore --- calculates the chi square score of a given alpha probability
*STD
*CALL
        AlphaToChiSquareScore(alphaProbability, degreesOfFreedom)

*PARMS
{alphaProbability} -- an alpha probability
{degreesOfFreedom} -- the degrees of freedom

*DESC
This procedure calculates the chi square score of a given probability.

*E.G.
In> AlphaToChiSquareScore(.1,4)
Result: 7.779440287

*SEE ChiSquareScoreToAlpha, ChiSquareTest
%/mathpiper_docs