%mathpiper,def="UnitsLessThan?"

RulebaseHoldArguments("UnitsLessThan?", ["left", "right"]);

5  ## UnitsLessThan?(a_ ∠ b_, c_ ∠ d_) <-- UnitsLessThan?(a, c) &? UnitsLessThan?(b, d);

10 ## UnitsLessThan?(a_~u_, b_~v_) <--
{
    DimensionsOf(u) =? DimensionsOf(v) &?
    NM(ValueOf(ConvertUnits(a~u, v) - b~v) <?  0);
}

20 ## UnitsLessThan?(left_, right_) <--
{
    Check(Type(left) =? "~", "The left operand (" + left + ") does not have a unit specified.");
    Check(Type(right) =? "~", "The right operand does not have a unit specified.");
}

%/mathpiper





%mathpiper_docs,name="UnitsLessThan?",categories="Mathematics Procedures,Units"
*CMD UnitsLessThan? --- test if one unit is less than another unit

*CALL
        UnitsLessThan?(x,y)

*PARMS

{x,y} -- units to compare


*DESC

This command compares two units. It returns {True} if {x} is less than
{y}. Otherwise, it returns {False}.


*EXAMPLES

*SEE UnitsGreaterThan?, UnitsEqual?
%/mathpiper_docs





%mathpiper,name="UnitsLessThan?",subtype="automatic_test"

Verify(UnitsLessThan?(2~mA, 5~mA),True);

Verify(UnitsLessThan?(5~mA, 3~mA),False);

Verify(UnitsLessThan?(3~V  ∠ 20~deg, 4~V ∠ 80~deg),True);

%/mathpiper





%mathpiper,name="UnitsLessThan?",subtype="in_prompts"

UnitsLessThan?(2~mA, 5~mA) -> True

UnitsLessThan?(5~mA, 3~mA) -> False

UnitsLessThan?(3~V  ∠ 20~deg, 4~V ∠ 80~deg) -> True

UnitsLessThan?(10~V ∠ 30~deg, 5~V ∠ 20~deg) -> False

%/mathpiper