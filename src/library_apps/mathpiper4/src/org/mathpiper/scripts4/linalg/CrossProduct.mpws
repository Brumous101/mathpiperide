%mathpiper,def="CrossProduct"

Procedure("CrossProduct",["aLeft", "aRight"])
{
    Local(length);
    length := Length(aLeft);
    
    Check(length =? 3, "Arguments vectors are not of dimension 3");
    Check(length =? Length(aRight), "Argument vectors are not of the same dimension");
    
    Local(perms);
    perms := PermutationsList([1,2,3]);
    
    Local(result);
    result := ZeroVector(3);
    
    Local(term);
    
    ForEach(term,perms)
    {
      result[ term[1] ] := result[ term[1] ] +
        LeviCivita(term) * aLeft[ term[2] ] * aRight[ term[3] ] ;
    }
    
    result;
}

%/mathpiper





%mathpiper_docs,name="CrossProduct",categories="Mathematics Procedures,Linear Algebra"
*CMD CrossProduct --- outer product of vectors

*CALL
    CrossProduct(a,b)


*PARMS

{a}, {b} -- three-dimensional vectors

*DESC

The cross product of the vectors "a"
and "b" is returned. The result is perpendicular to both "a" and
"b" and its length is the product of the lengths of the vectors.
Both "a" and "b" have to be three-dimensional.

*E.G.

In> CrossProduct([_a,_b,_c], [_d,_e,_f])
Result: [_b*_f - _c*_e,_c*_d - _a*_f,_a*_e - _b*_d]

*SEE InnerProduct
%/mathpiper_docs





%mathpiper,name="CrossProduct",subtype="automatic_test"

Verify(CrossProduct([1,2,3] , [4,2,5]) , [4,7,-6]);

%/mathpiper