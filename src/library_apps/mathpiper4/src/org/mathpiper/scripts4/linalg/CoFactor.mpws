%mathpiper,def="CoFactor"

Procedure("CoFactor",["matrix","ii","jj"])
{
  Local(perms,indices,result);
  indices:=BuildList(i,i,1,Length(matrix),1);
  perms:=PermutationsList(indices);
  result:=0;
  ForEach(item,perms)
     Decide(item[ii] =? jj,
       result:=result+
         Product(i,1,Length(matrix),
         Decide(ii=?i,1,matrix[i][item[i] ])
                  )*LeviCivita(item));
  result;
}

%/mathpiper

    %output,mpversion=".204",preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="CoFactor",categories="Mathematics Procedures,Linear Algebra"
*CMD CoFactor --- cofactor of a matrix
*STD
*CALL
        CoFactor(M,i,j)

*PARMS

{M} -- a matrix

{i}, {j} - positive integers

*DESC

{CoFactor} returns the cofactor of a matrix around
the element ($i$, $j$). The cofactor is the minor times
$(-1)^{i+j}$.

*E.G.

In> A := [[1,2,3], [4,5,6], [7,8,9]];
Result: [[1,2,3],[4,5,6],[7,8,9]];

In> UnparseMath2D(A);
        
        /                    \
        | ( 1 ) ( 2 ) ( 3 )  |
        |                    |
        | ( 4 ) ( 5 ) ( 6 )  |
        |                    |
        | ( 7 ) ( 8 ) ( 9 )  |
        \                    /
Result: True;

In> CoFactor(A,1,2);
Result: 6;

In> Minor(A,1,2);
Result: -6;

In> Minor(A,1,2) * (-1)^(1+2);
Result: 6;

*SEE Minor, Determinant, Inverse
%/mathpiper_docs







%mathpiper,name="CoFactor",subtype="automatic_test"

{
    Local(ll);
    ll:=[ [1,2,3],
          [2,-1,4],
          [3,4,3]
        ];

     Verify(NM(CoFactor(ll,1,2)),6);
}

%/mathpiper