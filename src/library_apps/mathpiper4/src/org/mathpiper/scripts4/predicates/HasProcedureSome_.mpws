%mathpiper,def="HasProcedureSome?"

/// procedure name given as string.
10 ## HasProcedureSome?(expr_, string_String?, looklist_) <-- HasProcedureSome?(expr, ToAtom(string), looklist);

/// procedure given as atom.
// atom contains no procedures
10 ## HasProcedureSome?(expr_Atom?, atom_Atom?, looklist_) <-- False;

// a list contains the procedure "List" so we test it together with procedures
// a procedure contains itself, or maybe an argument contains it
//
// first deal with procedures that do not belong to the list: return top level procedure
15 ## HasProcedureSome?(expr_Procedure?, atom_Atom?, looklist_)::(!? Contains?(looklist, ToString(Type(expr)))) <-- Equal?(First(ProcedureToList(expr)), atom);

// procedure belongs to the list - check its arguments
20 ## HasProcedureSome?(expr_Procedure?, atom_Atom?, looklist_) <-- Equal?(First(ProcedureToList(expr)), atom) |? ListHasProcedureSome?(Rest(ProcedureToList(expr)), atom, looklist);

%/mathpiper



%mathpiper_docs,name="HasProcedureSome?",categories="Programming Procedures,Predicates"
*CMD HasProcedureSome? --- check for expression containing a procedure
*STD
*CALL
        HasProcedureSome?(expr, func, list)

*PARMS

{expr} -- an expression

{func} -- a procedure atom to be found

{list} -- list of procedure atoms to be considered "transparent"

*DESC

The command {HasProcedureSome?} does the same thing as {HasProcedure?}, 
except it only looks at arguments of a given {list} of procedures. 
Arguments of all other procedures become "opaque" (as if they do not 
contain anything).

Note that since the operators "{+}" and "{-}" are prefix as well as 
infix operators, it is currently required to use {ToAtom("+")} to 
obtain the unevaluated atom "{+}".

*E.G.

In> HasProcedureSome?([a+b*2,c/d],/,[List])
Result: True;

In> HasProcedureSome?([a+b*2,c/d],*,[List])
Result: False;

*SEE HasProcedure?, HasProcedureArithmetic?, ProcedureList, VarList, HasExpression?
%/mathpiper_docs