%mathpiper,def="Diagonal?"

Diagonal?(A_Matrix?) <--
{
    Local(i,j,m,n,result);
    m      := Length(A);
    n      := Length(A[1]);
    result := (m =? n);    //    must be a square matrix
    
    i := 2;
    
    While(i <=? m &? result)
     {
         j := 1;
         While(j <=? n &? result)
         {
                 result := (i =? j |? A[i][j] =? 0);
                 
                 j++;
         }
         i++;
     }
     
     Decide(m =? 2, { result := result &? (A =? Transpose(A)); });
     
     result;
}

%/mathpiper





%mathpiper_docs,name="Diagonal?",categories="Programming Procedures,Predicates"
*CMD Diagonal? --- test for a diagonal matrix

*CALL
    Diagonal?(A)

*PARMS

{A} -- a matrix

*DESC

{Diagonal?(A)} returns {True} if {A} is a diagonal square matrix and {False} otherwise.

*E.G.
In> Diagonal?(IdentityMatrix(5))
Result: True;

In> Diagonal?(HilbertMatrix(5))
Result: False;
%/mathpiper_docs





%mathpiper,name="Diagonal?",subtype="automatic_test"

Verify(Diagonal?(IdentityMatrix(5)),True);

%/mathpiper