%mathpiper,def="AssociationDelete"

/// Delete an element of an associative list.
LocalSymbols(hash, key, element, hashexpr)
{

/// AssociationDelete(hash,["key", value])
10 ## AssociationDelete(hash_List?, element_List?) <--
{
        Local(index);
        index := Find(hash, element);
        Decide(
                index >? 0,
                Delete!(hash, index)
        );
        index>?0;        // return False if nothing found

}


/// AssociationDelete(hash, "key")
20 ## AssociationDelete(hash_List?, key_String?) <--
{
        AssociationDelete(hash, BuiltinAssociation(key, hash));
}

30 ## AssociationDelete(hash_List?, None) <-- False;

//HoldArgument("AssociationDelete", "hash");
//UnFence("AssociationDelete", 1);
//UnFence("AssociationDelete", 2);

}        // LocalSymbols(hash, ...)

%/mathpiper



%mathpiper_docs,name="AssociationDelete",categories="Programming Procedures,Lists (Operations)"
*CMD AssociationDelete --- delete an entry in an association list
*STD
*CALL
        AssociationDelete(alist, "key")
        AssociationDelete(alist, [key, value])

*PARMS

{alist} -- association list

{"key"} -- string, association key

{value} -- value of the key to be deleted

*DESC

The key {"key"} in the association list {alist} is deleted. (The list itself is modified.) If the key was found and successfully deleted, returns {True}, otherwise if the given key was not found, the procedure returns {False}.

The second, longer form of the procedure deletes the entry that has both the
specified key and the specified value. It can be used for two purposes:
*        1. to make sure that we are deleting the right value;
*        2. if several values are stored on the same key, to delete the specified entry (see the last example).

At most one entry is deleted.

*E.G.

In> writer := [];
Result: [];

In> writer["Iliad"] := "Homer";
Result: True;

In> writer["Henry IV"] := "Shakespeare";
Result: True;

In> writer["Ulysses"] := "James Joyce";
Result: True;

In> AssociationDelete(writer, "Henry IV")
Result: True;

In> AssociationDelete(writer, "Henry XII")
Result: False;

In> writer
Result: [["Ulysses","James Joyce"],
          ["Iliad","Homer"]];

In> Append!(writer,
          ["Ulysses", "Dublin"]);
Result: [["Iliad","Homer"],["Ulysses","James Joyce"],
          ["Ulysses","Dublin"]];

In> writer["Ulysses"];
Result: "James Joyce";

In> AssociationDelete(writer,["Ulysses","James Joyce"]);
Result: True;

In> writer
Result: [["Iliad","Homer"],["Ulysses","Dublin"]];


*SEE Association, AssociationIndices, AssociationValues
%/mathpiper_docs