/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.parsers;

import java.util.HashMap;


public class Metadata extends HashMap<String, Object> {
    static Metadata ofAtom(MathPiperParser aParser)
    {
        Metadata m = new Metadata();
        m.put("\"lineNumber\"", aParser.iTokenLineNumber);
        m.put("\"startIndex\"", aParser.iTokenStartIndex);
        m.put("\"endIndex\"",   aParser.iTokenEndIndex);
        m.put("\"sourceName\"", aParser.iInput.iStatus.getSourceName());
        
        return m;
    }
    
    static Metadata ofOpenAtom(MathPiperParser aParser)
    {
        Metadata m = new Metadata();
        
        m.put("\"openBraceLineNumber\"", aParser.iTokenLineNumber);
        m.put("\"openBraceStartIndex\"", aParser.iTokenStartIndex);
        m.put("\"openBraceEndIndex\"",   aParser.iTokenEndIndex);
        m.put("\"sourceName\"", aParser.iInput.iStatus.getSourceName());
        
        return m;
    }
    
    static Metadata ofCloseAtom(MathPiperParser aParser, Metadata aOpenMetadata)
    {        
        aOpenMetadata.put("\"closeBraceLineNumber\"", aParser.iTokenLineNumber);
        aOpenMetadata.put("\"closeBraceStartIndex\"", aParser.iTokenStartIndex);
        aOpenMetadata.put("\"closeBraceEndIndex\"",   aParser.iTokenEndIndex);
        aOpenMetadata.put("\"sourceName\"", aParser.iInput.iStatus.getSourceName());
        
        return aOpenMetadata;
    }
}
