package org.mathpiper.lisp.astprocessors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;


public class ObjectToMetaSubstitute
        implements ASTProcessor {

    Environment iEnvironment;
    private Map options;

    public ObjectToMetaSubstitute(Environment aEnvironment, Map options) {
        iEnvironment = aEnvironment;
        this.options = options;
    }


    public Cons patternMatches(Environment aEnvironment, int aStackTop, Cons aElement, List<Integer> positionList)
            throws Throwable {
	
	if(aElement instanceof NumberCons)
	{
	    return null;
	}
	
	
	if(aElement.car() instanceof String)
	{
	    String atomName = ((String) aElement.car());
            
	    if(! atomName.contains("_") && aEnvironment.isVariable(atomName))
	    {
                if(! ((Boolean) options.get("PatternVariables?")))
                {
                    atomName = "_".concat(atomName);
                }
                else
                {
                    atomName = atomName.concat("_");
                }
            }
            else if(! atomName.contains("$") && aEnvironment.isProcedure(atomName) && (Boolean) options.get("Procedures?"))
            {
                if(atomName.equals("List"))
                {
                    if((Boolean) options.get("Lists?"))
                    {
                        atomName = atomName.concat("$");
                    }
                }
                else
                {
                    atomName = atomName.concat("$");
                }
            }
	    
            Cons newCons = AtomCons.getInstance(aEnvironment.getPrecision(), atomName);
            
            if(aElement.getMetadataMap() != null)
            {
                newCons.setMetadataMap(new HashMap<String,Object>(aElement.getMetadataMap()));
            }
            
	    return(newCons);
	}

        return null;
    }

}
