/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.lisp.parametermatchers;

import java.util.ArrayDeque;
import java.util.Deque;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;

public class BlankSequencePatternParameterMatcher extends PatternParameterMatcher {
    // Index of variable in MathPiperPatternPredicateBase.iVariables.
    protected int iVarIndex;
    protected boolean iIsNullSequence;
    
    public BlankSequencePatternParameterMatcher(int aVarIndex, boolean isNullSequence) {
        super();
        
	iVarIndex = aVarIndex;
        iIsNullSequence   = isNullSequence;
    }
    
    @Override
    public boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons aExpressionTree, Cons[] patternVariableValues) throws Throwable {
        if (patternVariableValues[iVarIndex] == null) {
            Cons variable = Utility.flatCopy(aEnvironment, aStackTop, aExpressionTree);
            patternVariableValues[iVarIndex] = new SublistCons(aEnvironment.iListAtom.copy(false));
            ((Cons)patternVariableValues[iVarIndex].car()).setCdr(variable);
            Deque<Cons> variableStack = new ArrayDeque();

            Cons variableTraverser = variable;

            while (variableTraverser != null) {
                variableStack.push(variableTraverser);
                variableTraverser = variableTraverser.cdr();
            }

            Cons[] variableValuesBackup = patternVariableValues.clone();
            // I would add null to variableStack but null isn't allowed in deque
            if (getNextMatcher().argumentMatches(aEnvironment, aStackTop, null, variableValuesBackup)) {
                if (iIsNullSequence || !variableStack.isEmpty()) {
                    System.arraycopy(variableValuesBackup, 0, patternVariableValues, 0, variableValuesBackup.length); 
                    
                    return true;
                }
            }

            while (!variableStack.isEmpty()) {
                Cons rest = variableStack.pop();

                if (variableStack.isEmpty()) {
                    ((Cons)patternVariableValues[iVarIndex].car()).setCdr(null);
                } else {
                    variableStack.peek().setCdr(null); // remove rest from variable
                }

                variableValuesBackup = patternVariableValues.clone();
                
                boolean nextMatch = getNextMatcher().argumentMatches(aEnvironment, aStackTop, rest, variableValuesBackup);

                if (nextMatch) {
                    System.arraycopy(variableValuesBackup, 0, patternVariableValues, 0, variableValuesBackup.length);
                    return true;
                } else if (!variableStack.isEmpty()) {
                    variableStack.peek().setCdr(rest); // put rest back if no match found
                }
            }

            return false; // Note: the last iteration of the loop, rest equals aExpressionTree

        } else {
            Cons variableTraverser = ((Cons)patternVariableValues[iVarIndex].car()).cdr(); // Skip "List"

            while (variableTraverser != null) {
                if (aExpressionTree == null || ! Utility.equals(aEnvironment, aStackTop, variableTraverser, aExpressionTree))
                {
                    return false;
                }

                variableTraverser = variableTraverser.cdr();
                aExpressionTree = aExpressionTree.cdr();
            }

            return getNextMatcher().argumentMatches(aEnvironment, aStackTop, aExpressionTree, patternVariableValues);
        }
    }
    
    @Override
    public boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons[] aExpressionTree, int aLocation, Cons[] patternVariableValues) throws Throwable {
        if (aExpressionTree == null) {
            return getNextMatcher().argumentMatches(aEnvironment, aStackTop, aExpressionTree, aLocation, patternVariableValues);
        }
        
        LispError.throwError(aEnvironment, aStackTop, "Argument Matches on Arrays not implemented");
        
        return false;
    }

    @Override
    public String getType() {
        if (iIsNullSequence)
            return "NullBlankSequence";
        else
            return "BlankSequence";
    }
    
    @Override
    public String toString() {
        if (iIsNullSequence)
            return "___";
        else
            return "__";
    }
}
