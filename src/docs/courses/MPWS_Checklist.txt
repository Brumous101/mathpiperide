Mathpiper Worksheet Checklist
============================================

1.  There are 7 empty lines between each %group
    fold.

2.  There are 7 empty lines between every
    %mathpiper and %mathpiper_grade fold.

3.  Each problem instruction has been formatted
    using the format paragraph shortcut <ctrl>+e
    then f.

4.  Verify solutions and auto-grade code using 
    the <ctrl>+<alt>+<shift>+<enter> on empty line at 
    end of worksheet.

5.  All unpreserved folds have been removed using
    right click and selecting the "Remove 
    Unpreserved Folds" option in the popup menu.